Feature: Check Brands Page

  #This Scenario is to validate below test cases
  #1. Verify the info "I" mark is removed for How is My Brand Performing?
  @regression2
  Scenario: Login Successfully TestCaseIds 457997
    Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    Then Click on Brands Tab
    Then Verify the popup window message for "Sales,Units,Stores,Dollar Share,Trip Incidence,Sales per Trip,Units per Trip,Sales per Store,Units per Store"
