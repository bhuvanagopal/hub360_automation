Feature: Check Login functionality

  #This Scenario is to validate below test cases
  #1. Launch the Catalina HUB360 in Chrome browsers
  #2. Validate with valid UserName and Password - Hub360 application
  @regression
  Scenario Outline: Login Successfully TestCaseIds 69519,69521
    Given Navigate to "ProdUrl" and login as User "<User>"
    Then Verify whether the login is successful for Client "<Client>"
    Then Logout

    Examples: 
      | User         | Client       |
      | CatalinaUser | ActiveClient |
      | AdminUser    | ActiveClient |

  #This Scenario is to validate below test cases
  #1. Validate with Invalid UserName and Password - Hub360 application
  @regression
  Scenario Outline: Invalid USername and Password TestCaseIds 69560
    Given Navigate to "ProdUrl"
    Then Enter UserName "<User>" and password "<Password>"
    And Click on Login
    Then Verify for the error message "<ErrorMessage>" for invalid username and password

    Examples: 
      | User     | Password  | ErrorMessage                                        |
      | bgopal   | abcdjsljd | The username or password you provided is incorrect. |
      | Demo     | Catalina1 | The username or password you provided is incorrect. |
      | Catalina | Cat1      | The username or password you provided is incorrect. |

  #This Scenario is to validate below test cases
  #1. Verify Switch Client functionality
  #2. Verify Switch Client Close button functionality
  #3. Verify the Campaign information for different Clients
  @regression
  Scenario Outline: Switch Client functionality TestCaseIds 69561,69686,70795
    Given Navigate to "ProdUrl" and login as User "<User>"
    Then Verify whether the login is successful for Client "ActiveClient"
    And Verify Switch Client functionality for User "<User>" and Client "Halo Top"
    #Then Verify whether the login is successful for Client "Halo Top"
    Then Verify Switch Client Close button functionality for User "<User>" and Client "Carmalabs (Carmex)"
    And Verify Switch Client functionality for User "<User>" and Client "Twinings Tea"

    Examples: 
      | User         |
      | CatalinaUser |
      | AdminUser    |
