Feature: Check MultiTouchAttributR Page

  #This Scenario is to validate below test cases
  #1. Verify Campaign picker
  #2. Verify that Expand and hide all Campaigns functionality under Campaign Name
  #3. Verify the count of UPCs and Channels under link 'Expand'
  @regression
  Scenario: Verify Campaign picker TestCaseIds 69687,69858,69877
    Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    Then Verify whether the login is successful for Client "ActiveClient"
    And Verify Columns in the table for Page "Campaign Picker" "[Campaign Name, Start Date, End Date, UPCs, Channels]"
    And Verify Expand Link for UPC and Channels Column
    Then Verify the count of Campaign list and Campaign Names

  #This Scenario is to validate below test cases
  #1. Verify Campaign Inspector - New to Brand section - Bar Chart details
  @regression
  Scenario: Verify Graphs for New To Brand Tab TestCaseIds 69994
    Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    Then Verify whether the login is successful for Client "ActiveClient"
    Then Verify New To Brand tab with bar graph validation
    And Verify Graphs are present in Page "New To Brand"
    And Verify Info symbol, title "New to Brand, New to Category" and body "This report tells you how many Buyers who saw your campaign are new to your brand and category (have not purchased in past 26 weeks). These Buyers are counted daily." in Page "New To Brand"

  #This Scenario is to validate below test cases
  #1. Verify the Download CSV file
  @regression
  Scenario: Verify Download CSV file TestCaseIds 70389
    Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    Then Verify whether the login is successful for Client "ActiveClient"
    Then Verify the Download CSV file functionality for tab "New To Brand"
    And Verify the Download CSV file functionality for tab "Trial and Repeat"

  #This Scenario is to validate below test cases
  #1. Verify the Trial+Repeat functionality
  @regression
  Scenario: Verify Graphs for Trial+Repeat Tab TestCaseIds 70390
    Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    Then Verify whether the login is successful for Client "ActiveClient"
    And Verify Graphs are present in Page "Trial and Repeat"
    And Verify Info symbol, title "Trial and Repeat" and body "This is the count of Buyers that have tried one or more of your new UPCs, and how many times they have repeated over the course of the campaign. These Triers and Repeaters are counted daily." in Page "Trial and Repeat"

  #This Scenario is to validate below test cases
  #1. Verify the Recency Functionality
  #2. Verify the pop up information window for Recency tab for Channels, Buyer, S/Unit, S/Buyer, Unit/Buyer, Trip/Buyer, S/Trip
  @regression
  Scenario: Verify the Recency Functionality TestCaseIds 70493,70495
    Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    Then Verify whether the login is successful for Client "ActiveClient"
    And Verify Info symbol, title "Recency Report" and body "Our MTA algorithm applies fractional Buyer credit to the channels in which that Buyer was exposed to media – basically de-duping the Buyer count across channels. We are currently using a time decay model to attribution a Buyer across channels." in Page "Recency"
    Then Verify Columns in the table for Page "Recency" "[Channel, Buyers, $ / Unit, $ / Buyer, Unit / Buyer, Trip / Buyer, $ / Trip]"
    And Verify pop up information window for "Recency" tab

  #This Scenario is to validate below test cases
  #1. Verify the Channel functionality
  #2. Verify the pop up information window under Performance by Channel for Channel tab
  #3. Verify the Distribution by Buyers pie chart under Channels Tab
  @regression
  Scenario: Verify the Channel functionality TestCaseIds 70780,70781,70784
    Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    Then Verify whether the login is successful for Client "ActiveClient"
    And Verify Info symbol, title "Performance by Channel" and body "These are the media channels currently measured by the Catalina pixel – mobile (web and in app), desktop, and Catalina in-store network. Coming soon: Connected and Linear TV. Please note the "Unclassified" category includes all buyers for which not all information was available at the time of the report." in Page "Channel"
    And Verify Graphs are present in Page "Channel"
    Then Verify Columns in the table for Page "Channel" "[Channel, Impressions, Buyers, I/B Index*]"
    And Verify pop up information window for "Channel" tab
    And Verify the Distribution by Buyers data under Channels Tab

  #This Scenario is to validate below test cases
  #1. Verify that Creative functionality
  #2. Verify the pop up information window under Performance by Creative for Creative tab
  #3. Verify the Distribution by Buyers pie chart under Creative Tab
  #4. Verify the "View" link under Creative column under Percentage by Creative table
  @regression
  Scenario: Verify that Creative functionality TestCaseIds 70785,70786,70788,70789
    Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    Then Verify whether the login is successful for Client "ActiveClient"
    And Verify Info symbol, title "Performance by Creative" and body "Campaign metrics by creative and device are commonly reported for digital campaigns. This allows brands to know what creatives and devices are resulting in Buyers. Please note, at this time, we are not de-duping this information for Buyers who see multiple creatives or use multiple devices." in Page "Creative"
    And Verify Graphs are present in Page "Creative"
    Then Verify Columns in the table for Page "Creative" "[Description, Creative, Impressions, Buyers, I/B Index*]"
    And Verify pop up information window for "Creative" tab
    And Verify the Distribution by Buyers data under Creative Tab
    Then Verify View button in Creative Tab under Creative Column

  #This Scenario is to validate below test cases
  #1. Verify the Device functionality
  #2. Verify the pop up information window under Performance by Device for Device tab
  #3. Verify the Distribution by Buyers pie chart under Device Tab
  @regression
  Scenario: Verify that Creative functionality TestCaseIds 70790,70791,70792
    Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    Then Verify whether the login is successful for Client "ActiveClient"
    And Verify Info symbol, title "Performance by Device" and body "Campaign metrics by creative and device are commonly reported for digital campaigns. This allows brands to know what creatives and devices are resulting in Buyers. Please note, at this time, we are not de-duping this information for Buyers who see multiple creatives or use multiple devices. Also note, the "Unclassified" category includes all buyers for which not all information was available at the time of the report." in Page "Device"
    And Verify Graphs are present in Page "Device"
    Then Verify Columns in the table for Page "Device" "[Device, Impressions, Buyers, I/B Index*]"
    And Verify pop up information window for "Device" tab
    And Verify the Distribution by Buyers data under Device Tab

  #This Scenario is to validate below test cases
  #1. Verify the UPCs functionality
  #3. Verify the Download CSV file for UPCs tab
  @regression
  Scenario: Verify that UPCs functionality TestCaseIds 101355,101358
    #Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    #Then Verify whether the login is successful for Client "ActiveClient"
    #And Verify Info symbol, title "Promoted UPCs" and body "This is the list of promoted UPCs considered for this campaign. This list is used to count buyers who have seen impressions from the campaign and have purchased one or more of the promoted UPCs." in Page "UPCs"
    #Then Verify Columns in the table for Page "UPCs" "[UPC, Brand, Description, New UPC?, Category, Product Type, Attribute, Size]"
    #Then Verify the Download CSV file functionality for tab "UPCs"
    And Verify UPC details in UI with Source data

  #This Scenario is to validate below test cases
  #1. Verify the Timeframe for the selected client and campaign
  @regression
  Scenario: Verify the Timeframe for the selected client and campaign TestCaseIds 70793
    Given Navigate to "ProdUrl" and login as User "CatalinaUser"
    Then Verify whether the login is successful for Client "ActiveClient"
    And Change the Calendar TimeFrame to date range which is out of range
    Then Verify error message "No data for this tab was found You can try adjusting the date range, retailer, or campaign"
