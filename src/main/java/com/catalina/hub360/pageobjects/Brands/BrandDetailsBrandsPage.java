package com.catalina.hub360.pageobjects.brands;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.catalina.hub360.utils.Constant;


public class BrandDetailsBrandsPage {
 
	WebDriver driver;

	@FindBy (xpath="//div[@class='kpi-title']/span")
	public WebElement BrandTitle_BrandsPage;
	
	@FindBy (xpath="//p[@class='h4 ml-3 pl-3 font-weight-light']")
	public WebElement JustifyContentTitle_BrandsPage;
	
	@FindBy (xpath="(//select[@id='filterDropdown'])[1]")
	public WebElement FilterDropDown_BrandsPage;
	
	@FindBy (xpath="(//select[@id='filterDropdown'])[1]/option")
	public List<WebElement> FilterDropDownList_BrandsPage;
	
	@FindBy (xpath="//p[@class='h4 ml-3 pl-3 font-weight-light']/app-info/div/span")
	public WebElement InfoCircle_JustifyContent_BrandsPage;
	
	@FindBy (xpath="//p[@class='h4 ml-3 pl-3 font-weight-light']")
	public WebElement TrendsTitle_BrandsPage;
	
	@FindBy (xpath="//h5[@class='modal-title']")
	public WebElement ModalTitle_BrandsPage;
	
	@FindBy (xpath="//div[@class='modal-body']/p")
	public WebElement ModalBody_BrandsPage;
	
	@FindBy (xpath="//button[contains(text(),'Done')]")
	public WebElement DoneButton_InfoPopUp_BrandsPage;
	

	public BrandDetailsBrandsPage(WebDriver driver)  throws IOException
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
}
