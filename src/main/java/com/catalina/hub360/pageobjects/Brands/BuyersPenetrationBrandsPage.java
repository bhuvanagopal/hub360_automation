package com.catalina.hub360.pageobjects.brands;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.hub360.utils.Constant;
import com.catalina.hub360.utils.ExcelUtility;
import com.catalina.hub360.utils.FileReadLocation;

public class BuyersPenetrationBrandsPage {

	WebDriver driver;

	@FindBy (xpath="//span[@class='info-circle']")
	public WebElement InfoSymbol_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='col-12 col-md-5']/p")
	public WebElement TotalBuyers_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='key-label-number']")
	public List<WebElement> BuyersCount_MultiTouchAttributRPage;

	@FindBy (xpath="(//div[@class='key-label-title'])[1]")
	public WebElement ExistingBuyers_MultiTouchAttributRPage;

	@FindBy (xpath="(//div[@class='key-label-title'])[2]")
	public WebElement NewToBrand_MultiTouchAttributRPage;

	@FindBy (xpath="(//div[@class='key-label-title'])[3]")
	public WebElement NewToBrandCategory_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='modal-header']/h5")
	public WebElement ModalHeader_InfoPopUp_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='modal-body']")
	public WebElement ModalBody_InfoPopUp_MultiTouchAttributRPage;

	@FindBy (xpath="//button[@class='btn btn-outline-secondary']")
	public WebElement DoneButton_InfoPopUp_MultiTouchAttributRPage;

	@FindBy (xpath="//canvas[@id='chartsjs']")
	public List<WebElement> Graphs_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='brand-button-container']/button")
	public WebElement DownloadCSV_Button_MultiTouchAttributRPage;

	@FindBy (xpath="//table[@class='table table-striped text-left']/thead/tr/th/span/span")
	public List<WebElement> ColumnNames_MultiTouchAttributRPage;

	@FindBy (xpath="//*[@class='col-12 creatives-table']/table/thead/tr/th/span/span")
	public List<WebElement> ColumnNames_CreativeTab_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[1]")
	public WebElement ChannelsInfoPopUp_RecencyReport_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[2]")
	public WebElement BuyersInfoPopUp_RecencyReport_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[3]")
	public WebElement Unit$InfoPopUp_RecencyReport_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[4]")
	public WebElement Buyer$InfoPopUp_RecencyReport_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[5]")
	public WebElement UnitBuyerInfoPopUp_RecencyReport_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[6]")
	public WebElement TripBuyerInfoPopUp_RecencyReport_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[7]")
	public WebElement Trip$InfoPopUp_RecencyReport_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[8]")
	public WebElement TotalsInfoPopUp_RecencyReport_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[1]")
	public WebElement ImpressionsInfo_Channel_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[2]")
	public WebElement BuyersInfo_Channel_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='info-circle light'])[3]")
	public WebElement IBindexInfo_Channel_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='tag-combo-legend-title tab-label-small']")
	public WebElement DistributionByBuyers_Title_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='tag-combo-legend-label']")
	public List<WebElement> DistributionByBuyers_Data_MultiTouchAttributRPage;

	@FindBy (xpath="(//table[@class='table table-striped text-left fixed-header'])[2]/tbody/tr/td[1]")
	public List<WebElement> DistributionByBuyers_DatainTable_Creative_MultiTouchAttributRPage;

	@FindBy (xpath="//table[@class='table table-striped text-left']/tbody/tr/td[1]")
	public List<WebElement> DistributionByBuyers_DatainTable_Channel_MultiTouchAttributRPage;

	@FindBy (xpath="//table[@class='table table-striped text-left']/tbody/tr/td[1]")
	public List<WebElement> DistributionByBuyers_DatainTable_Device_MultiTouchAttributRPage;

	@FindBy (xpath="((//table[@class='table table-striped text-left fixed-header'])[2]/tbody/tr/td[2]/a)[1]")
	public WebElement ViewLink_CreativeTab_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@id='beeswax-ad-wrapper']")
	public WebElement Image_ViewLink_CreativeTab_MultiTouchAttributRPage;


	public BuyersPenetrationBrandsPage(WebDriver driver)  throws IOException
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}

	String downLoadedFileName;
	BrandShiftingBrandsPage insightAndCampaignMultiTouchAttributRPage;
	BrandDetailsBrandsPage campaignInspectorMultiTouchAttributRPage;

	public void closeBrowser() {
		driver.quit();
	} 

	public boolean graphDisplayed(String page)
	{
		try
		{
			List<String> width = new ArrayList<String>();
			List<String> height = new ArrayList<String>();
			List<String> style = new ArrayList<String>();

			for(WebElement graphs : Graphs_MultiTouchAttributRPage)
			{
				width.add(graphs.getAttribute("width"));
				height.add(graphs.getAttribute("height"));
				style.add(graphs.getAttribute("style"));
			}

			switch(page)
			{
			case "New To Brand":
				Assert.assertEquals(width.toString(), "[358, 867]");
				Assert.assertEquals(height.toString(), "[358, 432]");
				Assert.assertEquals(style.toString(), "[display: block; height: 239px; width: 239px;, display: block; height: 288px; width: 578px;]");
				return true;

			case "Trial and Repeat":
				Assert.assertEquals(width.toString(), "[372, 370, 742]");
				Assert.assertEquals(height.toString(), "[372, 370, 247]");
				Assert.assertEquals(style.toString(), "[display: block; height: 248px; width: 248px;, display: block; height: 247px; width: 247px;, display: block; height: 165px; width: 495px;]");
				return true;

			case "By Category":
				Assert.assertEquals(width.toString(), "[1486]");
				Assert.assertEquals(height.toString(), "[297]");
				Assert.assertEquals(style.toString(), "[display: block; height: 198px; width: 991px;]");
				return true;

			case "Channel":
				Assert.assertEquals(width.toString(), "[480]");
				Assert.assertEquals(height.toString(), "[480]");
				Assert.assertEquals(style.toString(), "[display: block; height: 320px; width: 320px;]");
				return true;

			case "Device":
				Assert.assertEquals(width.toString(), "[480]");
				Assert.assertEquals(height.toString(), "[480]");
				Assert.assertEquals(style.toString(), "[display: block; height: 320px; width: 320px;]");
				return true;

			case "Creative":
				Assert.assertEquals(width.toString(), "[646]");
				Assert.assertEquals(height.toString(), "[646]");
				Assert.assertEquals(style.toString(), "[display: block; height: 431px; width: 431px;]");
				return true;

			default:
				return false;
			}
		}
		catch(Exception e)
		{
			return false;
		}
	}



	public boolean verifyInfoPopUp(String columnName)
	{
		String title;
		String body;
		switch(columnName)
		{
		case "Channel":
			ChannelsInfoPopUp_RecencyReport_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.ChannelInfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.ChannelInfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;

		case "Buyers":
			BuyersInfoPopUp_RecencyReport_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.BuyerInfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.BuyerInfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;

		case "Unit$":
			Unit$InfoPopUp_RecencyReport_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.Unit$InfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.Unit$InfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;

		case "Buyer$":
			Buyer$InfoPopUp_RecencyReport_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.Buyer$InfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.Buyer$InfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;

		case "UnitBuyer":
			UnitBuyerInfoPopUp_RecencyReport_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.UnitBuyerInfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.UnitBuyerInfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;

		case "TripBuyer":
			TripBuyerInfoPopUp_RecencyReport_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.TripBuyerInfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.TripBuyerInfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;

		case "Trip$":
			Trip$InfoPopUp_RecencyReport_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.Trip$InfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			System.out.println(body);
			Assert.assertEquals(Constant.Trip$InfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;

		case "Totals":
			TotalsInfoPopUp_RecencyReport_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.TotalInfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.TotalInfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;

		case "Impressions":
			ImpressionsInfo_Channel_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.ImpressionsInfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.ImpressionsInfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;

		case "BuyersChannel":
			BuyersInfoPopUp_RecencyReport_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.BuyersInfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.BuyersInfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;

		case "IBindex":
			IBindexInfo_Channel_MultiTouchAttributRPage.click();
			title = ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.IBindexInfoHeader, title);
			body = ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
			Assert.assertEquals(Constant.IBindexInfoBody, body);
			DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
			return true;


		default:
			return false;

		}
	}


}
