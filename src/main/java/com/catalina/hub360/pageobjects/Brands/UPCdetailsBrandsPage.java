package com.catalina.hub360.pageobjects.brands;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UPCdetailsBrandsPage {

	WebDriver driver;

	@FindBy (xpath="//p[@class='ml-3 pr-3 my-0 border-right border-white']")
	public WebElement ClientName_MultiTouchAttributRPage;

	@FindBy (xpath="//*[@class='row no-gutters']")
	public WebElement Insights_MultiTouchAttributRPage;

	@FindBy (xpath="//*[@id='tabHeader']")
	public WebElement TabHeader_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='text-left']")
	public WebElement ActiveCampaigns_MultiTouchAttributRPage;

	@FindBy (xpath="//a[contains(text(),'Expand and compare all active campaigns')]")
	public WebElement ExpandCampaignNameList_MultiTouchAttributRPage;

	@FindBy (xpath="//a[contains(text(),'Hide all active campaigns')]")
	public WebElement HideCampaignNameList_MultiTouchAttributRPage;

	@FindBy (xpath="//button[@class='btn btn-outline-secondary mx-2 bg-transparent current-campaign']")
	public WebElement ActiveCampaignNameLabel_MultiTouchAttributRPage;

	@FindBy (xpath="(//a[@class='table-link'])[1]")
	public WebElement ActiveCampaignNameList_MultiTouchAttributRPage;

	@FindBy (xpath="//table[@class='table table-striped text-left fixed-header']/tbody/tr")
	public List<WebElement> ActiveCampaignListTable_MultiTouchAttributRPage;

	@FindBy (xpath="//table[@class='table table-striped text-left fixed-header']/thead/tr/th")
	public List<WebElement> ActiveCampaignTableHeader_MultiTouchAttributRPage;

	@FindBy (xpath="//i[@class='fa fa-angle-right']")
	public WebElement RightMoveButton_MultiTouchAttributRPage;

	@FindBy (xpath="(//a[@class='table-link'])[2]")
	public WebElement UPCExpandLink_MultiTouchAttributRPage;

	@FindBy (xpath="(//td[@class='bg-vanilla text-nowrap']/span)[3]")
	public WebElement UPCcount_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='modal-body']/ul/li")
	public List<WebElement> UPCList_MultiTouchAttributRPage;

	@FindBy (xpath="(//td[@class='bg-vanilla text-nowrap']/span)[4]")
	public WebElement ChannelsCount_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='modal-body']/ul/li")
	public List<WebElement> ChannelsList_MultiTouchAttributRPage;

	@FindBy (xpath="(//a[@class='table-link'])[3]")
	public WebElement ChannelExpandLink_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='modal-header']/h5[contains(text(),'UPC')]")
	public WebElement UPCPopUpHeader_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='modal-header']/button[@class='close']")
	public WebElement CloseButtonPopUp_MultiTouchAttributRPage;

	@FindBy (xpath="//div[@class='modal-header']/h5[contains(text(),'Channels')]")
	public WebElement ChannelPopUpHeader_MultiTouchAttributRPage;

	@FindBy (xpath="(//button[@id='showDropdown'])[1]")
	public WebElement BrandName_MultiTouchAttributRPage;

	@FindBy (xpath="//button[@class='btn btn-outline-secondary px-3 btn-outline-secondary-dark']")
	public WebElement TimeFrame_Button_MultiTouchAttributRPage;

	@FindBy (xpath="(//span[@class='custom-day selected'])[2]")
	public WebElement EndDate_TimeFrame_MultiTouchAttributRPage;
	
	@FindBy (xpath="(//div[@class='ngb-dp-month'])[2]//span[@class='custom-day']")
	public List<WebElement> CalendarSecondWidget_TimeFrame_MultiTouchAttributRPage;
	


	public UPCdetailsBrandsPage(WebDriver driver)  throws IOException
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}

	String downLoadedFileName;

	public void closeBrowser() {
		driver.quit();
	} 

	public int count(List<WebElement> webElement)
	{
		int count = 0;
		for(WebElement list : webElement)
		{
			count++;
		}
		System.out.println("Count : "+count);
		return count;
	}

	public void selectInvalidTimeFrame() throws InterruptedException
	{
		TimeFrame_Button_MultiTouchAttributRPage.click();
		String endDate = EndDate_TimeFrame_MultiTouchAttributRPage.getText();
		EndDate_TimeFrame_MultiTouchAttributRPage.click();
		System.out.println(endDate);
		String newDate;
		if(endDate.equalsIgnoreCase("30") || endDate.equalsIgnoreCase("31"))
		{
			newDate = "1";
		}
		else
		{
			newDate = String.valueOf(Integer.parseInt(endDate)+1);
		}
		System.out.println(newDate);

		List<WebElement> selectCemMerchantsOptions = CalendarSecondWidget_TimeFrame_MultiTouchAttributRPage;
		for (WebElement m : selectCemMerchantsOptions) {
			if (m.getText().equalsIgnoreCase(newDate)){
				m.click();
				break;
			}
		}

	}


		//Verifying Element is present or not

		//	public boolean isElementPresent(String Path) throws InterruptedException, IOException {
		//
		//		List<WebElement> dynamicElement = driver.findElements(By.xpath(Path));
		//		if(dynamicElement.size() != 0)
		//		{
		//			System.out.println("Element present");
		//		}
		//		else
		//		{
		//			System.out.println("Element not present");
		//		}
		//		return true;
		//	}
		//
		//
		//	public List<String> userDeatils() {
		//
		//
		//		List<WebElement> nameTable = driver.findElements(By.xpath("//*[@class='aseEmail']"));   
		//		List<String> emailList = new ArrayList<String>();
		//
		//		int index=0;
		//		for(int i=0;i<nameTable.size();i++) { 
		//			WebElement element=nameTable.get(i);
		//			String name = element.getText(); 
		//			emailList.add(index,name);
		//		}
		//
		//		return emailList;
		//	}
		//	public void mouseoverclick()
		//	{
		//
		//		Actions builder = new Actions(driver);
		//		builder.moveToElement(Emailfiled_AdminPage).build().perform();
		//		builder.moveToElement(logout_AdminPage).build().perform();
		//		logout_AdminPage.click();
		//
		//	}




	}


