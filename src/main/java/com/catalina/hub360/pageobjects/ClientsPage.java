package com.catalina.hub360.pageobjects;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.catalina.hub360.utils.ObjectActions;



public class ClientsPage {

	WebDriver driver;
	ObjectActions oa;

	@FindBy (xpath="//h5[contains(text(),'Catalina Clients')]")
	public WebElement ClientsHeader_ClientsPage;
	
	@FindBy (xpath="//input[@placeholder='Search']")
	public WebElement SearchField_ClientsPage;

	@FindBy (xpath="//tr[@class='client-list-item']")
	public WebElement ClientList_ClientsPage;
	
	@FindBy (xpath="(//td[@class='client-list-item-prop'])[1]")
	public WebElement ClientName_ClientsPage;

	@FindBy (xpath="(//td[@class='client-list-item-prop'])[2]")
	public WebElement ClientUserId_ClientsPage;
	
	@FindBy (xpath="(//td[@class='client-list-item-prop'])[3]")
	public WebElement ClientUrl_ClientsPage;

	@FindBy (xpath="(//td[@class='client-list-item-prop'])[4]")
	public WebElement ClientRole_ClientsPage;
	
	@FindBy (xpath="(//button[@class='btn btn-light btn-sm'])[1]")
	public WebElement LaunchClientButton_ClientsPage;

	@FindBy (xpath="(//button[@class='btn btn-light btn-sm'])[2]")
	public WebElement EditClientButton_ClientsPage;
	
	@FindBy (xpath="//button[@class='btn btn-light btn-sm removeClientButton']")
	public WebElement RemoveClientButton_ClientsPage;
	


	public ClientsPage(WebDriver driver)  throws IOException
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}

}


