package com.catalina.hub360.pageobjects;


import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.hub360.pageobjects.multitouchattributr.InsightAndCampaignMultiTouchAttributRPage;
import com.catalina.hub360.utils.ReadPropertyFile;

public class LoginPage {
	WebDriver driver;
	ReadPropertyFile readPropertyFile;

	@FindBy (xpath="//div[@class='w-100 mb-5 pr-5']")
	public WebElement CatalinaHub360_LoginPage;

	@FindBy (xpath="//input[@formcontrolname='username']")
	public WebElement UserName_Text_LoginPage;

	@FindBy (xpath="//input[@formcontrolname='password']")
	public WebElement Password_Text_LoginPage;

	@FindBy (xpath="//button[@type='submit']")
	public WebElement Submit_Btn_LoginPage;

	@FindBy (xpath="//div[@id='userDropdown']/span[@class='nav-profile-name']")
	public WebElement ProfileName_LoginPage;

	@FindBy (xpath="//div[@class='nav-profile-dropdown dropdown-menu show']/a[contains(text(), 'Logout')]")
	public WebElement LogoutButton_LoginPage;

	@FindBy (xpath="//div[@class='login-form-error']")
	public WebElement ErrorMessageForInvalidLogin_LoginPage;

	@FindBy (xpath="//div[@class='nav-profile-dropdown dropdown-menu show']/a[contains(text(), 'Switch Client')]")
	public WebElement SwitchClientButton_LoginPage;

	@FindBy (xpath="//div[@class='search-container']")
	public WebElement SelectActiveClientSearchBox_LoginPage;

	@FindBy (xpath="//div[@class='list-item']")
	public List<WebElement> ClientList_LoginPage;

	@FindBy (xpath="//button[contains(text(), 'Set Active Client')]")
	public WebElement SetActiveClientButton_LoginPage;

	@FindBy (xpath="//button[contains(text(), 'Close')]")
	public WebElement CloseSetActiveClientPopUp_LoginPage;

	@FindBy (xpath="//div[@class='col-12 bg-dark text-center text-white position-fixed z-100']/p/button")
	public WebElement ExitButton_LoginPage;

	@FindBy (xpath="(//span[@class='nav-item-name' and contains(text(), 'Brands')])[1]")
	public WebElement Brands_LoginPage;

	@FindBy (xpath="(//span[@class='nav-item-name' and contains(text(), 'Multi-Touch AttributR')])[1]")
	public WebElement MultiTouchAttributR_LoginPage;
	
	@FindBy (xpath="//div[contains(text(), 'September 2019')]")
	public WebElement SeptemberMonth_TimeFrame_LoginPage;
	
	@FindBy (xpath="//button[@title='Previous month']")
	public WebElement PreviousMonth_ArrowKey_TimeFrame_LoginPage;
	
	@FindBy (xpath="//div[@aria-label='Sunday, September 1, 2019']")
	public WebElement SeptemberMonth_Date_TimeFrame_LoginPage;
	
	@FindBy (xpath="//div[@aria-label='Wednesday, October 16, 2019']")
	public WebElement OctoberMonth_Date_TimeFrame_LoginPage;



	public LoginPage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	ClientsPage clientsPage;
	InsightAndCampaignMultiTouchAttributRPage insightAndCampaignMultiTouchAttributRPage;

	public boolean LoginAs(String Role) throws InterruptedException, IOException, NoSuchElementException {
		try {
			readPropertyFile = new ReadPropertyFile();
			switch (Role) {

			case "CatalinaUser":				
				UserName_Text_LoginPage.sendKeys(readPropertyFile.getCatalinaUserName());
				Password_Text_LoginPage.sendKeys(readPropertyFile.getCatalinaPassword());
				Submit_Btn_LoginPage.click();
				System.out.println("Action : "+Role+" Login, PASS");
				return true;

			case "AdminUser":				
				UserName_Text_LoginPage.sendKeys(readPropertyFile.getAdminUserName());
				Password_Text_LoginPage.sendKeys(readPropertyFile.getAdminPassword());
				Submit_Btn_LoginPage.click();
				System.out.println("Action : "+Role+" Login, PASS");
				return true;



			default:
				return false;
			}
		} catch (Exception e) {
			System.out.println("Exception :" +Role+" Login, Fail --"+ e.getMessage());
			return false;
		}
	}

	public void switchClientFunctionality(String user, String client) throws InterruptedException, IOException
	{
		if(user.equalsIgnoreCase("AdminUser"))
		{
			try
			{
				(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(ExitButton_LoginPage));
				ExitButton_LoginPage.click();
			}
			catch(Exception e)
			{
				clientsPage = new ClientsPage(driver);
				(new WebDriverWait(driver,5)).until(ExpectedConditions.visibilityOf(clientsPage.ClientsHeader_ClientsPage));
				clientsPage.ClientsHeader_ClientsPage.isDisplayed();
				System.out.println("Admin login");
				(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(clientsPage.ClientName_ClientsPage));
				clientsPage.SearchField_ClientsPage.sendKeys(client);
				Thread.sleep(3000);
				String clientName = clientsPage.ClientName_ClientsPage.getText();
				Assert.assertEquals(client, clientName);
				Actions action = new Actions(driver);
				WebElement we = clientsPage.ClientName_ClientsPage;
				action.moveToElement(we).moveToElement(clientsPage.LaunchClientButton_ClientsPage).click().build().perform();

				insightAndCampaignMultiTouchAttributRPage =  new InsightAndCampaignMultiTouchAttributRPage(driver);
				(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(insightAndCampaignMultiTouchAttributRPage.Insights_MultiTouchAttributRPage));
				insightAndCampaignMultiTouchAttributRPage.Insights_MultiTouchAttributRPage.isDisplayed();
				String selectedClientName = insightAndCampaignMultiTouchAttributRPage.ClientName_MultiTouchAttributRPage.getText();
				Assert.assertEquals(client, selectedClientName);
			}
		}
		if(user.equalsIgnoreCase("CatalinaUser"))
		{
			try
			{
				(new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOf(Brands_LoginPage));
				Brands_LoginPage.click();
				(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(ExitButton_LoginPage));
				ExitButton_LoginPage.click();
			}
			catch(Exception e)
			{
				System.out.println("Not an admin login");
			}
			(new WebDriverWait(driver,40)).until(ExpectedConditions.visibilityOf(ProfileName_LoginPage));
			ProfileName_LoginPage.click();
			(new WebDriverWait(driver,40)).until(ExpectedConditions.visibilityOf(SwitchClientButton_LoginPage));
			SwitchClientButton_LoginPage.click();
			(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(SetActiveClientButton_LoginPage));
			SelectActiveClientSearchBox_LoginPage.click();
			for(WebElement selectClient : ClientList_LoginPage)
			{
				String clientName = selectClient.getText();
				if(clientName.equalsIgnoreCase(client))
				{
					Thread.sleep(3000);
					selectClient.click();
					Thread.sleep(3000);
					break;
				}
			}
			(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(SetActiveClientButton_LoginPage));
			SetActiveClientButton_LoginPage.click();
			Thread.sleep(3000);
			insightAndCampaignMultiTouchAttributRPage = new InsightAndCampaignMultiTouchAttributRPage(driver);
			Boolean verification = false;
			MultiTouchAttributR_LoginPage.click();
			Thread.sleep(2000);
			String clientName = insightAndCampaignMultiTouchAttributRPage.ClientName_MultiTouchAttributRPage.getText();
			System.out.println(clientName);
			if(clientName.equalsIgnoreCase(client))
			{
				verification = true;
			}
			else
			{
				verification = false;
			}
			Assert.assertEquals(verification, true);
		}
	}

	public void switchClientCloseButtonFunctionality(String user, String client) throws InterruptedException, IOException
	{
		if(user.equalsIgnoreCase("AdminUser"))
		{
			try
			{
				(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(ExitButton_LoginPage));
				ExitButton_LoginPage.click();
			}
			catch(Exception e)
			{
				clientsPage = new ClientsPage(driver);
				(new WebDriverWait(driver,5)).until(ExpectedConditions.visibilityOf(clientsPage.ClientsHeader_ClientsPage));
				clientsPage.ClientsHeader_ClientsPage.isDisplayed();
				System.out.println("Admin login");
				(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(clientsPage.ClientName_ClientsPage));
				clientsPage.SearchField_ClientsPage.sendKeys(client);
				Thread.sleep(3000);
				String clientName = clientsPage.ClientName_ClientsPage.getText();
				Assert.assertEquals(client, clientName);
				Actions action = new Actions(driver);
				WebElement we = clientsPage.ClientName_ClientsPage;
				action.moveToElement(we).moveToElement(clientsPage.LaunchClientButton_ClientsPage).click().build().perform();

				insightAndCampaignMultiTouchAttributRPage =  new InsightAndCampaignMultiTouchAttributRPage(driver);
				(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(insightAndCampaignMultiTouchAttributRPage.Insights_MultiTouchAttributRPage));
				insightAndCampaignMultiTouchAttributRPage.Insights_MultiTouchAttributRPage.isDisplayed();
				(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(insightAndCampaignMultiTouchAttributRPage.TabHeader_MultiTouchAttributRPage));
				insightAndCampaignMultiTouchAttributRPage.TabHeader_MultiTouchAttributRPage.isDisplayed();
				String selectedClientName = insightAndCampaignMultiTouchAttributRPage.ClientName_MultiTouchAttributRPage.getText();
				Assert.assertEquals(client, selectedClientName);
			}
		}
		if(user.equalsIgnoreCase("CatalinaUser"))
		{
			try
			{
				(new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOf(Brands_LoginPage));
				Brands_LoginPage.click();
				(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(ExitButton_LoginPage));
				ExitButton_LoginPage.click();
			}
			catch(Exception e)
			{
				(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(ProfileName_LoginPage));
				ProfileName_LoginPage.click();
				(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(SwitchClientButton_LoginPage));
				SwitchClientButton_LoginPage.click();
				(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(SetActiveClientButton_LoginPage));
				SelectActiveClientSearchBox_LoginPage.click();
				for(WebElement selectclient : ClientList_LoginPage)
				{
					String clientName = selectclient.getText();
					if(clientName.equalsIgnoreCase(client))
					{
						Thread.sleep(3000);
						selectclient.click();
						Thread.sleep(3000);
						break;
					}
				}
				(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(CloseSetActiveClientPopUp_LoginPage));
				CloseSetActiveClientPopUp_LoginPage.click();
				Thread.sleep(3000);
				insightAndCampaignMultiTouchAttributRPage = new InsightAndCampaignMultiTouchAttributRPage(driver);
				(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(insightAndCampaignMultiTouchAttributRPage.BrandName_MultiTouchAttributRPage));
				Boolean verification;
				String clientName = insightAndCampaignMultiTouchAttributRPage.BrandName_MultiTouchAttributRPage.getText();
				System.out.println(clientName);
				if(clientName.contains(client))
				{
					verification = false;
				}
				else
				{
					verification = true;
				}
				Assert.assertEquals(verification, true);
			}
		}
	}
}
