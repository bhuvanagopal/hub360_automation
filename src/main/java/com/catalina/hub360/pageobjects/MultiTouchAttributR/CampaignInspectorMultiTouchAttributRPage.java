package com.catalina.hub360.pageobjects.multitouchattributr;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CampaignInspectorMultiTouchAttributRPage {

	WebDriver driver;

	@FindBy (xpath="//a[@class='nav-link active']/div[contains(text(), 'New to Brand')]")
	public WebElement NewToBrandTab_MultiTouchAttributRPage;
	
	@FindBy (xpath="//a[@class='nav-link active']/div[contains(text(), 'Trial + Repeat')]")
	public WebElement TrialAndRepeatTab_MultiTouchAttributRPage;
	
	@FindBy (xpath="//a[@class='nav-link active']/div[contains(text(), 'By Category')]")
	public WebElement ByCategoryTab_MultiTouchAttributRPage;
	
	@FindBy (xpath="//a[@class='nav-link active']/div[contains(text(), 'Recency')]")
	public WebElement RecencyTab_MultiTouchAttributRPage;
	
	@FindBy (xpath="//a[@class='nav-link active']/div[contains(text(), 'Channel')]")
	public WebElement ChannelTab_MultiTouchAttributRPage;
	
	@FindBy (xpath="//a[@class='nav-link active']/div[contains(text(), 'Creative')]")
	public WebElement CreativeTab_MultiTouchAttributRPage;
	
	@FindBy (xpath="//a[@class='nav-link active']/div[contains(text(), 'Device')]")
	public WebElement DeviceTab_MultiTouchAttributRPage;
	
	@FindBy (xpath="//a[@class='nav-link active']/div[contains(text(), 'UPCs')]")
	public WebElement UPCsTab_MultiTouchAttributRPage;
	
	@FindBy (xpath="//h3[@class='text-center m5-5']")
	public WebElement NoDataMeaasge_MultiTouchAttributRPage;
	
	@FindBy (xpath="(//div[@class='col-12'])[3]")
	public WebElement NoData_ErrorMessage_MultiTouchAttributRPage;
	

	public CampaignInspectorMultiTouchAttributRPage(WebDriver driver)  throws IOException
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);	
	}
	
	GraphsAndDataMultiTouchAttributRPage graphsAndDataMultiTouchAttributRPage;
	

	public void navigateToPage(String page) throws IOException
	{
		if(page.equalsIgnoreCase("New To Brand"))
		{
		NewToBrandTab_MultiTouchAttributRPage.click();
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(graphsAndDataMultiTouchAttributRPage.InfoSymbol_MultiTouchAttributRPage));
		}
		else if(page.equalsIgnoreCase("Campaign Picker"))
		{
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(graphsAndDataMultiTouchAttributRPage.InfoSymbol_MultiTouchAttributRPage));
		}
		else if(page.equalsIgnoreCase("Trial and Repeat"))
		{
		TrialAndRepeatTab_MultiTouchAttributRPage.click();
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(graphsAndDataMultiTouchAttributRPage.InfoSymbol_MultiTouchAttributRPage));
		}
		else if(page.equalsIgnoreCase("Recency"))
		{
		RecencyTab_MultiTouchAttributRPage.click();
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(graphsAndDataMultiTouchAttributRPage.InfoSymbol_MultiTouchAttributRPage));
		}
		else if(page.equalsIgnoreCase("Channel"))
		{
		ChannelTab_MultiTouchAttributRPage.click();
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(graphsAndDataMultiTouchAttributRPage.InfoSymbol_MultiTouchAttributRPage));
		}
		else if(page.equalsIgnoreCase("Creative"))
		{
		CreativeTab_MultiTouchAttributRPage.click();
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(graphsAndDataMultiTouchAttributRPage.InfoSymbol_MultiTouchAttributRPage));
		}
		else if(page.equalsIgnoreCase("Device"))
		{
		DeviceTab_MultiTouchAttributRPage.click();
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(graphsAndDataMultiTouchAttributRPage.InfoSymbol_MultiTouchAttributRPage));
		}
		else if(page.equalsIgnoreCase("UPCs"))
		{
		UPCsTab_MultiTouchAttributRPage.click();
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(graphsAndDataMultiTouchAttributRPage.InfoSymbol_MultiTouchAttributRPage));
		}
	}
}


