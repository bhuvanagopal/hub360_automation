package com.catalina.hub360.slack;

public class Example {

	public static void slackMessage(String testRunMessage) {

		SlackMessage slackMessage = new SlackMessage("HUB360_TestRun", "Notification : "+testRunMessage+" ", "", "qa-cellfire-automation");
		SlackUtils.sendMessage(slackMessage);

		System.out.println("////////////////////////////////////////////SUCCESSFULLY UPDATED IN SLACK/////////////////");
	}

}