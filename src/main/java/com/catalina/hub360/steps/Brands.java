package com.catalina.hub360.steps;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.hub360.pageobjects.LoginPage;
import com.catalina.hub360.pageobjects.brands.BrandDetailsBrandsPage;
import com.catalina.hub360.utils.Constant;
import com.catalina.hub360.utils.SharedResource;

import cucumber.api.java.en.Then;

public class Brands {

	WebDriver driver;
	LoginPage loginPage;
	BrandDetailsBrandsPage brandDetailsBrandsPage;

	public Brands(SharedResource sharedResource) throws IOException{
		this.driver = sharedResource.init();
		brandDetailsBrandsPage = new BrandDetailsBrandsPage(driver);
	}

	@Then("^Click on Brands Tab$")
	public void click_on_Brands_Tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		loginPage = new LoginPage(driver);
		(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(loginPage.Brands_LoginPage));
		loginPage.Brands_LoginPage.click();
		brandDetailsBrandsPage = new BrandDetailsBrandsPage(driver);
		try
		{
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.visibilityOf(brandDetailsBrandsPage.BrandTitle_BrandsPage));
		}
		catch(Exception e)
		{
			driver.navigate().refresh();
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.visibilityOf(brandDetailsBrandsPage.BrandTitle_BrandsPage));
		}
		brandDetailsBrandsPage.BrandTitle_BrandsPage.isDisplayed();
		String title = brandDetailsBrandsPage.BrandTitle_BrandsPage.getText();
		Assert.assertEquals(title, "How Is My Brand Performing?");
	}

	@Then("^Verify the popup window message for \"([^\"]*)\"$")
	public void verify_the_popup_window_message_for(String tab) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		brandDetailsBrandsPage.InfoCircle_JustifyContent_BrandsPage.click();
		Assert.assertEquals(brandDetailsBrandsPage.ModalTitle_BrandsPage.getText(), Constant.SalesTitle);
		Assert.assertEquals(brandDetailsBrandsPage.ModalBody_BrandsPage.getText().replaceAll("\n", ""), Constant.SalesBody);
		brandDetailsBrandsPage.DoneButton_InfoPopUp_BrandsPage.click();
		String[] list  = tab.split(",");
		for(int i=0; i<list.length; i++)
		{
			brandDetailsBrandsPage.FilterDropDown_BrandsPage.click();
			System.out.println(list[i]);
			for (WebElement e : brandDetailsBrandsPage.FilterDropDownList_BrandsPage) {
				if (e.getText().contains(list[i]))
				{
					e.click();
					break;
				}
			}
			(new WebDriverWait(driver, 20)).until(ExpectedConditions.visibilityOf(brandDetailsBrandsPage.TrendsTitle_BrandsPage));
			String lines[] = brandDetailsBrandsPage.TrendsTitle_BrandsPage.getText().split("\\r?\\n");
			String title = lines[0];
			String expectedTitle  = "What Are My Brand's "+list[i]+" Trends?";
			System.out.println(title);
			System.out.println(expectedTitle);
			Assert.assertEquals(title, expectedTitle);
		}
		
	}


}
