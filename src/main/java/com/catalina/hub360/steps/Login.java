package com.catalina.hub360.steps;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.hub360.pageobjects.ClientsPage;
import com.catalina.hub360.pageobjects.LoginPage;
import com.catalina.hub360.pageobjects.multitouchattributr.CampaignInspectorMultiTouchAttributRPage;
import com.catalina.hub360.pageobjects.multitouchattributr.InsightAndCampaignMultiTouchAttributRPage;
import com.catalina.hub360.utils.FileUtil;
import com.catalina.hub360.utils.ReadPropertyFile;
import com.catalina.hub360.utils.SharedResource;
import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Login {

	WebDriver driver;
	LoginPage loginPage;
	InsightAndCampaignMultiTouchAttributRPage insightAndCampaignMultiTouchAttributRPage;
	CampaignInspectorMultiTouchAttributRPage campaignInspectorMultiTouchAttributRPage;
	ClientsPage clientsPage;
	ReadPropertyFile readPropertyFile;
	FileUtil fileUtil;
	String activeClient;

	public Login(SharedResource sharedResource){
		this.driver = sharedResource.init();
		loginPage = new LoginPage(driver);
	}

	public void getActiveClient(String client) throws IOException, InterruptedException
	{
		if(client.equalsIgnoreCase("ActiveClient"))
		{
			readPropertyFile = new ReadPropertyFile();
			this.activeClient = readPropertyFile.getActiveClient();
		}
		else
		{
			this.activeClient = client;
		}
		
	}

	@Given("^Navigate to \"([^\"]*)\"$")
	public void navigate_to(String url) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Navigating to HUB 360");
		readPropertyFile = new ReadPropertyFile();
		driver.navigate().to(readPropertyFile.getEnvironment(url));
	}

	@Given("^Enter UserName \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void enter_UserName_and_password(String userName, String password) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		Reporter.addStepLog("Entering UserName");
		loginPage.UserName_Text_LoginPage.sendKeys(userName);
		Reporter.addStepLog("Entering Password");
		loginPage.Password_Text_LoginPage.sendKeys(password);
		//		ScreenShot.addScreenshotToScreen(this.driver, "Before Login");
	}

	@Then("^Click on Login$")
	public void click_on_Login() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		loginPage.Submit_Btn_LoginPage.click();
		Reporter.addStepLog("Clicked on Login Button");
	}

	@Given("^Navigate to \"([^\"]*)\" and login as User \"([^\"]*)\"$")
	public void navigate_to_and_login_as_User(String url, String role) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		if (System.getProperty("Environment") == null) {
			navigate_to(url);
		} else {
			navigate_to(System.getProperty("Environment"));
		}
		loginPage.LoginAs(role);
		click_on_Login();
	}

	@Then("^Verify whether the login is successful for Client \"([^\"]*)\"$")
	public void verify_whether_the_login_is_successful_for_Client(String client) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		getActiveClient(client);
		Reporter.addStepLog("Selected Client : "+activeClient);
		try
		{
			clientsPage = new ClientsPage(driver);
			(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(clientsPage.ClientsHeader_ClientsPage));
			clientsPage.ClientsHeader_ClientsPage.isDisplayed();
			System.out.println("Admin login");
			(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(clientsPage.ClientName_ClientsPage));
			clientsPage.SearchField_ClientsPage.sendKeys(activeClient);
			Thread.sleep(3000);
			String clientName = clientsPage.ClientName_ClientsPage.getText();
			Assert.assertEquals(activeClient, clientName);
			Actions action = new Actions(driver);
			WebElement we = clientsPage.ClientName_ClientsPage;
			action.moveToElement(we).moveToElement(clientsPage.LaunchClientButton_ClientsPage).click().build().perform();
		
		}
		catch(Exception e)
		{
			System.out.println("Not an admin login");
		}
//		Thread.sleep(3000);
//		
//		insightAndCampaignMultiTouchAttributRPage = new InsightAndCampaignMultiTouchAttributRPage(driver);
//		insightAndCampaignMultiTouchAttributRPage.TimeFrame_Button_MultiTouchAttributRPage.click();
//		
//		Boolean done=false;
//		loginPage = new LoginPage(driver);
//		while(!done)
//		{
//			try
//			{
//			loginPage.SeptemberMonth_TimeFrame_LoginPage.isDisplayed();
//			done = true;
//			}
//			catch(Exception e)
//			{
//				loginPage.PreviousMonth_ArrowKey_TimeFrame_LoginPage.click();
//			}
//		}
//		
//		loginPage.SeptemberMonth_Date_TimeFrame_LoginPage.click();
//		loginPage.OctoberMonth_Date_TimeFrame_LoginPage.click();
		
		
		insightAndCampaignMultiTouchAttributRPage =  new InsightAndCampaignMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(insightAndCampaignMultiTouchAttributRPage.Insights_MultiTouchAttributRPage));
		insightAndCampaignMultiTouchAttributRPage.Insights_MultiTouchAttributRPage.isDisplayed();
		Thread.sleep(3000);
		try
		{
			insightAndCampaignMultiTouchAttributRPage =  new InsightAndCampaignMultiTouchAttributRPage(driver);
			(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(insightAndCampaignMultiTouchAttributRPage.TabHeader_MultiTouchAttributRPage));
			insightAndCampaignMultiTouchAttributRPage.TabHeader_MultiTouchAttributRPage.isDisplayed();
		}
		catch(Exception e)
		{
			System.out.println("No data found");
			campaignInspectorMultiTouchAttributRPage = new CampaignInspectorMultiTouchAttributRPage(driver);
			campaignInspectorMultiTouchAttributRPage.NoDataMeaasge_MultiTouchAttributRPage.isDisplayed();
			verify_Switch_Client_functionality_for_User_and_Client("CatalinaUser", activeClient);
			(new WebDriverWait(driver,20)).until(ExpectedConditions.visibilityOf(insightAndCampaignMultiTouchAttributRPage.TabHeader_MultiTouchAttributRPage));
			insightAndCampaignMultiTouchAttributRPage.TabHeader_MultiTouchAttributRPage.isDisplayed();
		}
		String clientName = insightAndCampaignMultiTouchAttributRPage.ClientName_MultiTouchAttributRPage.getText();
		Assert.assertEquals(activeClient, clientName);
		Reporter.addStepLog("Login is successfull for client " + activeClient );
		//		insightAndCampaignMultiTouchAttributRPage.RightMoveButton_MultiTouchAttributRPage.click();
		//		campaignInspectorMultiTouchAttributRPage = new CampaignInspectorMultiTouchAttributRPage(driver);
		//		(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(campaignInspectorMultiTouchAttributRPage.NewToBrandTab_MultiTouchAttributRPage));
	}

	@Then("^Logout$")
	public void logout() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		loginPage.ProfileName_LoginPage.click();
		loginPage.LogoutButton_LoginPage.click();
		Reporter.addStepLog("Logged Out from application");
	}

	@Then("^Verify for the error message \"([^\"]*)\" for invalid username and password$")
	public void verify_for_the_error_message_for_invalid_username_and_password(String error) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(loginPage.ErrorMessageForInvalidLogin_LoginPage));
		loginPage.ErrorMessageForInvalidLogin_LoginPage.isDisplayed();
		String errorMessage  = loginPage.ErrorMessageForInvalidLogin_LoginPage.getText();
		Assert.assertEquals(error, errorMessage);
		fileUtil = new FileUtil();
		readPropertyFile = new ReadPropertyFile();
		fileUtil.colorValidation(loginPage.ErrorMessageForInvalidLogin_LoginPage, readPropertyFile.getErrorMessageColor());
		Reporter.addStepLog("Login Failed with error message " + errorMessage);
	}

	@Then("^Verify Switch Client functionality for User \"([^\"]*)\" and Client \"([^\"]*)\"$")
	public void verify_Switch_Client_functionality_for_User_and_Client(String user, String client) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		loginPage.switchClientFunctionality(user, client);
		Reporter.addStepLog("Switching client to " + client);
	}

	@Then("^Verify Switch Client Close button functionality for User \"([^\"]*)\" and Client \"([^\"]*)\"$")
	public void verify_Switch_Client_Close_button_functionality_for_User_and_Client(String user, String client) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		loginPage.switchClientCloseButtonFunctionality(user, client);
		Reporter.addStepLog("Switching client to " + client);
	}

}
