package com.catalina.hub360.steps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.catalina.hub360.pageobjects.ClientsPage;
import com.catalina.hub360.pageobjects.LoginPage;
import com.catalina.hub360.pageobjects.multitouchattributr.CampaignInspectorMultiTouchAttributRPage;
import com.catalina.hub360.pageobjects.multitouchattributr.GraphsAndDataMultiTouchAttributRPage;
import com.catalina.hub360.pageobjects.multitouchattributr.InsightAndCampaignMultiTouchAttributRPage;
import com.catalina.hub360.utils.FileUtil;
import com.catalina.hub360.utils.ReadPropertyFile;
import com.catalina.hub360.utils.SharedResource;
import com.cucumber.listener.Reporter;

import cucumber.api.java.en.Then;

public class MultiTouchAttributR {

	WebDriver driver;
	LoginPage loginPage;
	InsightAndCampaignMultiTouchAttributRPage insightAndCampaignMultiTouchAttributRPage;
	CampaignInspectorMultiTouchAttributRPage campaignInspectorMultiTouchAttributRPage;
	GraphsAndDataMultiTouchAttributRPage graphsAndDataMultiTouchAttributRPage;
	ClientsPage clientsPage;
	FileUtil fileUtil;
	ReadPropertyFile readPropertyFile;

	public MultiTouchAttributR(SharedResource sharedResource) throws IOException{
		this.driver = sharedResource.init();
		insightAndCampaignMultiTouchAttributRPage = new InsightAndCampaignMultiTouchAttributRPage(driver);
	}

	@Then("^Verify Columns in the table for Page \"([^\"]*)\" \"([^\"]*)\"$")
	public void verify_Columns_in_the_table_for_Page(String page, String header) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		List<String> columnNames = new ArrayList<String>();
		campaignInspectorMultiTouchAttributRPage = new CampaignInspectorMultiTouchAttributRPage(driver);
		campaignInspectorMultiTouchAttributRPage.navigateToPage(page);
		if(page.equalsIgnoreCase("Campaign Picker"))
		{
			insightAndCampaignMultiTouchAttributRPage = new InsightAndCampaignMultiTouchAttributRPage(driver);
			for(WebElement names : insightAndCampaignMultiTouchAttributRPage.ActiveCampaignTableHeader_MultiTouchAttributRPage)
			{
				columnNames.add(names.getText());
			}
		}
		else if(page.equalsIgnoreCase("Recency") || page.equalsIgnoreCase("Channel") || page.equalsIgnoreCase("Device") || page.equalsIgnoreCase("UPCs"))
		{
			graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
			for(WebElement list : graphsAndDataMultiTouchAttributRPage.ColumnNames_MultiTouchAttributRPage)
			{
				columnNames.add(list.getText());
			}	
		}
		else if(page.equalsIgnoreCase("Creative"))
		{
			graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
			for(WebElement list : graphsAndDataMultiTouchAttributRPage.ColumnNames_CreativeTab_MultiTouchAttributRPage)
			{
				columnNames.add(list.getText());
			}	
		}
		System.out.println(columnNames);
		Assert.assertEquals(columnNames.toString(), header);
		Reporter.addStepLog("Verified Column Names in the table");
	}

	@Then("^Verify Expand Link for UPC and Channels Column$")
	public void verify_Expand_Link_for_UPC_and_Channels_Column() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		int count = 0;
		int upcCount = Integer.parseInt(insightAndCampaignMultiTouchAttributRPage.UPCcount_MultiTouchAttributRPage.getText());
		System.out.println("UPCcount : "+upcCount);
		insightAndCampaignMultiTouchAttributRPage.UPCExpandLink_MultiTouchAttributRPage.click();
		insightAndCampaignMultiTouchAttributRPage.UPCPopUpHeader_MultiTouchAttributRPage.isDisplayed();
		count = insightAndCampaignMultiTouchAttributRPage.count(insightAndCampaignMultiTouchAttributRPage.UPCList_MultiTouchAttributRPage);
		Assert.assertEquals(upcCount, count);
		insightAndCampaignMultiTouchAttributRPage.CloseButtonPopUp_MultiTouchAttributRPage.click();
		int channelsCount = Integer.parseInt(insightAndCampaignMultiTouchAttributRPage.ChannelsCount_MultiTouchAttributRPage.getText());
		System.out.println("ChannelsCount :"+channelsCount);
		insightAndCampaignMultiTouchAttributRPage.ChannelExpandLink_MultiTouchAttributRPage.click();
		insightAndCampaignMultiTouchAttributRPage.ChannelPopUpHeader_MultiTouchAttributRPage.isDisplayed();
		count = insightAndCampaignMultiTouchAttributRPage.count(insightAndCampaignMultiTouchAttributRPage.ChannelsList_MultiTouchAttributRPage);
		Assert.assertEquals(channelsCount, count);
		insightAndCampaignMultiTouchAttributRPage.CloseButtonPopUp_MultiTouchAttributRPage.click();
		Reporter.addStepLog("Verified Expand Link for UPC and Channels Column");
	}

	@Then("^Verify the count of Campaign list and Campaign Names$")
	public void verify_the_count_of_Campaign_list_and_Campaign_Names() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		int actualCount = Integer.parseInt(insightAndCampaignMultiTouchAttributRPage.ActiveCampaigns_MultiTouchAttributRPage.getText().replaceAll("[^0-9]", ""));
		System.out.println("CampaignCount : "+actualCount);
		insightAndCampaignMultiTouchAttributRPage.ExpandCampaignNameList_MultiTouchAttributRPage.click();
		int expectedCount = insightAndCampaignMultiTouchAttributRPage.count(insightAndCampaignMultiTouchAttributRPage.ActiveCampaignListTable_MultiTouchAttributRPage);
		System.out.println("CapaignCountInList : "+expectedCount);
		Assert.assertEquals(expectedCount, actualCount);
		Thread.sleep(3000);
		(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(insightAndCampaignMultiTouchAttributRPage.HideCampaignNameList_MultiTouchAttributRPage));
		insightAndCampaignMultiTouchAttributRPage.HideCampaignNameList_MultiTouchAttributRPage.click();
		try
		{
		for(int i=1; i<=actualCount; i++)
		{
			insightAndCampaignMultiTouchAttributRPage.RightMoveButton_MultiTouchAttributRPage.click();
			String actualActiveCampaign = insightAndCampaignMultiTouchAttributRPage.ActiveCampaignNameLabel_MultiTouchAttributRPage.getText();
			System.out.println("ActiveCampaignInLabel : "+actualActiveCampaign);
			String expectedActiveCampaign = insightAndCampaignMultiTouchAttributRPage.ActiveCampaignNameList_MultiTouchAttributRPage.getText();
			System.out.println("ActiveCampaignInList : "+expectedActiveCampaign);
			Assert.assertEquals(expectedActiveCampaign, actualActiveCampaign);
		}
		}
		catch(Exception e)
		{}
		Reporter.addStepLog("Verified count of Campaign list and Campaign Names");
	}

	@Then("^Verify New To Brand tab with bar graph validation$")
	public void verify_New_To_Brand_tab_with_bar_graph_validation() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		int totalCount = 0;
		campaignInspectorMultiTouchAttributRPage = new CampaignInspectorMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(campaignInspectorMultiTouchAttributRPage.NewToBrandTab_MultiTouchAttributRPage));
		campaignInspectorMultiTouchAttributRPage.NewToBrandTab_MultiTouchAttributRPage.click();
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		(new WebDriverWait(driver,10)).until(ExpectedConditions.visibilityOf(graphsAndDataMultiTouchAttributRPage.InfoSymbol_MultiTouchAttributRPage));
		String[] totalBuyersCount = graphsAndDataMultiTouchAttributRPage.TotalBuyers_MultiTouchAttributRPage.getText().split("\\(");
		int totalBuyers = Integer.parseInt(totalBuyersCount[0].replaceAll("[^0-9]",""));
		System.out.println(totalBuyers);
		for(WebElement list : graphsAndDataMultiTouchAttributRPage.BuyersCount_MultiTouchAttributRPage)
		{
			String[] count = list.getText().split("\\(");
			totalCount = totalCount + Integer.parseInt(count[0].replaceAll(" ", ""));
		}
		Assert.assertEquals(totalCount, totalBuyers);
		fileUtil = new FileUtil();
		readPropertyFile = new ReadPropertyFile();
		fileUtil.colorValidation(graphsAndDataMultiTouchAttributRPage.ExistingBuyers_MultiTouchAttributRPage, readPropertyFile.getExistingBuyersColor());
		fileUtil.colorValidation(graphsAndDataMultiTouchAttributRPage.NewToBrand_MultiTouchAttributRPage, readPropertyFile.getNewToBrandColor());
		fileUtil.colorValidation(graphsAndDataMultiTouchAttributRPage.NewToBrandCategory_MultiTouchAttributRPage, readPropertyFile.getNewToBrandAndCategoryColor());
		Reporter.addStepLog("Verified New To Brand tab with bar graph validation");
	}

	@Then("^Verify Graphs are present in Page \"([^\"]*)\"$")
	public void verify_Graphs_are_present_in_Page(String page) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		campaignInspectorMultiTouchAttributRPage =  new CampaignInspectorMultiTouchAttributRPage(driver);
		campaignInspectorMultiTouchAttributRPage.navigateToPage(page);
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
//		boolean verification = graphsAndDataMultiTouchAttributRPage.graphDisplayed(page);
//		Assert.assertEquals(verification, true);
		Reporter.addStepLog("Verified Graphs are present in Page " + page);
	}

	@Then("^Verify Info symbol, title \"([^\"]*)\" and body \"(.*)\" in Page \"([^\"]*)\"$")
	public void verify_Info_symbol_title_and_body_in_Page(String infoTitle, String infoBody, String page) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		campaignInspectorMultiTouchAttributRPage = new CampaignInspectorMultiTouchAttributRPage(driver);
		campaignInspectorMultiTouchAttributRPage.navigateToPage(page);
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		graphsAndDataMultiTouchAttributRPage.InfoSymbol_MultiTouchAttributRPage.click();
		String title = graphsAndDataMultiTouchAttributRPage.ModalHeader_InfoPopUp_MultiTouchAttributRPage.getText();
		Assert.assertEquals(infoTitle, title);
		String body = graphsAndDataMultiTouchAttributRPage.ModalBody_InfoPopUp_MultiTouchAttributRPage.getText();
		Assert.assertEquals(infoBody, body);
		graphsAndDataMultiTouchAttributRPage.DoneButton_InfoPopUp_MultiTouchAttributRPage.click();
		insightAndCampaignMultiTouchAttributRPage = new InsightAndCampaignMultiTouchAttributRPage(driver);
		insightAndCampaignMultiTouchAttributRPage.ClientName_MultiTouchAttributRPage.isDisplayed();
		Reporter.addStepLog("Verified Info symbol, title and body in Page " + page);
	}

	@Then("^Verify the Download CSV file functionality for tab \"([^\"]*)\"$")
	public void verify_the_Download_CSV_file_functionality_for_tab(String tab) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		Boolean verification = graphsAndDataMultiTouchAttributRPage.downloadCSVfile(tab);
		Assert.assertEquals(true, verification);
		Reporter.addStepLog("Verified the Download CSV file functionality for tab " + tab);
	}

	@Then("^Verify pop up information window for \"([^\"]*)\" tab$")
	public void verify_pop_up_information_window_for_tab(String tab) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		if(tab.equalsIgnoreCase("Recency"))
		{
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("Channel");
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("Buyers");
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("Unit$");
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("Buyer$");
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("UnitBuyer");
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("TripBuyer");
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("Trip$");
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("Totals");
		}
		else if(tab.equalsIgnoreCase("Channel") || tab.equalsIgnoreCase("Creative") || tab.equalsIgnoreCase("Device"))
		{
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("Impressions");
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("BuyersChannel");
			graphsAndDataMultiTouchAttributRPage.verifyInfoPopUp("IBindex");
		}
		Reporter.addStepLog("Verified pop up information window for tab " + tab);
	}

	@Then("^Verify the Distribution by Buyers data under Channels Tab$")
	public void verify_the_Distribution_by_Buyers_data_under_Channels_Tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		String title = graphsAndDataMultiTouchAttributRPage.DistributionByBuyers_Title_MultiTouchAttributRPage.getText();
		Assert.assertEquals(title, "Distribution by Buyers");
		List<String> labels = new ArrayList<String>();
		List<String> buyerLabels = new ArrayList<String>();
		for(WebElement names : graphsAndDataMultiTouchAttributRPage.DistributionByBuyers_DatainTable_Channel_MultiTouchAttributRPage)
		{
			if(!names.getText().equalsIgnoreCase("Unknown"))
			{
			buyerLabels.add(names.getText());
			}
		}
		System.out.println(buyerLabels);
		for(WebElement names : graphsAndDataMultiTouchAttributRPage.DistributionByBuyers_Data_MultiTouchAttributRPage)
		{
			labels.add(names.getText());
		}
		System.out.println(labels);
		for(int i = 0; i <labels.size(); i++)
		{
			Assert.assertEquals(labels.get(i), buyerLabels.get(i));
		}
		Reporter.addStepLog("Verified the Distribution by Buyers data under Channels Tab");
	}

	@Then("^Verify the Distribution by Buyers data under Creative Tab$")
	public void verify_the_Distribution_by_Buyers_data_under_Creative_Tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		String title = graphsAndDataMultiTouchAttributRPage.DistributionByBuyers_Title_MultiTouchAttributRPage.getText();
		Assert.assertEquals(title, "Distribution by Buyers");
		List<String> labels = new ArrayList<String>();
		List<String> buyerLabels = new ArrayList<String>();
		for(WebElement names : graphsAndDataMultiTouchAttributRPage.DistributionByBuyers_DatainTable_Creative_MultiTouchAttributRPage)
		{
			buyerLabels.add(names.getText());
		}
		System.out.println(buyerLabels);
		for(WebElement names : graphsAndDataMultiTouchAttributRPage.DistributionByBuyers_Data_MultiTouchAttributRPage)
		{
			labels.add(names.getText());
		}
		System.out.println(labels);
		for(int i = 0; i <5; i++)
		{
			Assert.assertEquals(labels.get(i), buyerLabels.get(i));
		}
		Reporter.addStepLog("Verified the Distribution by Buyers data under Creative Tab");
	}

	@Then("^Verify the Distribution by Buyers data under Device Tab$")
	public void verify_the_Distribution_by_Buyers_data_under_Device_Tab() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		String title = graphsAndDataMultiTouchAttributRPage.DistributionByBuyers_Title_MultiTouchAttributRPage.getText();
		Assert.assertEquals(title, "Distribution by Buyers");
		List<String> labels = new ArrayList<String>();
		List<String> buyerLabels = new ArrayList<String>();
		for(WebElement names : graphsAndDataMultiTouchAttributRPage.DistributionByBuyers_DatainTable_Device_MultiTouchAttributRPage)
		{
			buyerLabels.add(names.getText());
		}
		System.out.println(buyerLabels);
		for(WebElement names : graphsAndDataMultiTouchAttributRPage.DistributionByBuyers_Data_MultiTouchAttributRPage)
		{
			labels.add(names.getText());
		}
		System.out.println(labels);
		for(int i = 0; i <10; i++)
		{
			Assert.assertEquals(labels.get(i), buyerLabels.get(i));
		}
		Reporter.addStepLog("Verified the Distribution by Buyers data under Device Tab");
	}

	@Then("^Verify View button in Creative Tab under Creative Column$")
	public void verify_View_button_in_Creative_Tab_under_Creative_Column() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		graphsAndDataMultiTouchAttributRPage = new GraphsAndDataMultiTouchAttributRPage(driver);
		graphsAndDataMultiTouchAttributRPage.ViewLink_CreativeTab_MultiTouchAttributRPage.click();
		String mainWindow = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
			driver.switchTo().window(winHandle);
		}
		System.out.println(driver.getCurrentUrl());
		boolean verification = false;
		if(driver.getCurrentUrl().contains("https://catalina.api.beeswax.com/buzz/public/CreativePreview.php?token="))
		{
			verification=true;
		}
		Assert.assertEquals(true, verification);
		try
		{
		graphsAndDataMultiTouchAttributRPage.Image_ViewLink_CreativeTab_MultiTouchAttributRPage.isDisplayed();
		}
		catch(Exception e)
		{
		graphsAndDataMultiTouchAttributRPage.Console_ViewLink_CreativeTab_MultiTouchAttributRPage.isDisplayed();
		}
		driver.close();
		driver.switchTo().window(mainWindow);
		Reporter.addStepLog("Verified View button in Creative Tab under Creative Column");
	}

	@Then("^Change the Calendar TimeFrame to date range which is out of range$")
	public void change_the_Calendar_TimeFrame_to_date_range_which_is_out_of_range() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		insightAndCampaignMultiTouchAttributRPage = new InsightAndCampaignMultiTouchAttributRPage(driver);
		insightAndCampaignMultiTouchAttributRPage.selectInvalidTimeFrame();
		Reporter.addStepLog("Selected invalid Time Frame");
	}

	@Then("^Verify error message \"([^\"]*)\"$")
	public void verify_error_message(String message) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		campaignInspectorMultiTouchAttributRPage = new CampaignInspectorMultiTouchAttributRPage(driver);
		Thread.sleep(5000);
		(new WebDriverWait(driver, 20)).until(ExpectedConditions.visibilityOf(campaignInspectorMultiTouchAttributRPage.RecencyTab_MultiTouchAttributRPage));
		String errorMessage = campaignInspectorMultiTouchAttributRPage.NoData_ErrorMessage_MultiTouchAttributRPage.getText().replaceAll("\n", " ");
		System.out.println(errorMessage);
		System.out.println(message);
		Assert.assertEquals(message, errorMessage);
		Reporter.addStepLog("Verified error message " + errorMessage);
	}
	
	@Then("^Verify UPC details in UI with Source data$")
	public void verify_UPC_details_in_UI_with_Source_data() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
	}
}
