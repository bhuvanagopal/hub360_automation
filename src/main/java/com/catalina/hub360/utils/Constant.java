package com.catalina.hub360.utils;

public class Constant {

	public static String ChannelInfoHeader = "Channel";
	public static String ChannelInfoBody = "Type of support used for advertising (currently desktop, mobile web, mobile app and in­store).";
	public static String BuyerInfoHeader = "Buyers";
	public static String BuyerInfoBody = "Number of Buyers of one of the UPCs in the campaign.";
	public static String Unit$InfoHeader = "$ / Unit";
	public static String Unit$InfoBody = "Average price paid by shopper ID for promoted UPCs purchased as a result of themulti-channel campaign for each channel.";
	public static String Buyer$InfoHeader = "$ / Buyer";
	public static String Buyer$InfoBody = "Average total spend by Buyers for this campaign across all of the UPCs associated with this campaign.";
	public static String UnitBuyerInfoHeader = "Unit / Buyer";
	public static String UnitBuyerInfoBody = "Average units per Buyer for this campaign (of the UPCs in the campaign).";
	public static String TripBuyerInfoHeader = "Trip / Buyer";
	public static String TripBuyerInfoBody = "Average number of store trips of Buyers in this campaign since the campaign began.";
	public static String Trip$InfoHeader = "$ / Trip";
	public static String Trip$InfoBody = "Measures which channel generates the most $ per trip for promoted UPC considered as the campaign unfolds.";
	public static String TotalInfoHeader = "Totals";
	public static String TotalInfoBody = "This is the total Buyers across all channels. If a Buyer has been exposed to multiple channels, a fractional credit of that Buyer is applied to the channels they were exposed via a time decay model. This report is cumulative since the beginning of the campaign.";
	public static String ImpressionsInfoHeader = "Impressions";
	public static String ImpressionsInfoBody = "Served impressions (BuyerVision and where we can see 3rd party) plus number of in-store prints.";
	public static String BuyersInfoHeader = "Buyers";
	public static String BuyersInfoBody = "An ID’d Buyer who was served an impression associated with this campaign, then bought one of the campaign UPCs within the Catalina retailer network NOTE: this buyer count is not de-deduped. Buyer count by Channel is de-deduped on the Recency tab.";
	public static String IBindexInfoHeader = "I/B Index*";
	public static String IBindexInfoBody = "Impressions to Buyer Index. This is the ratio of impressions to buyers – a lower ratio means that it required less impressions to convert the Buyers.";
	public static String SalesTitle  = "What Are My Brand's Sales Trends?";
	public static String SalesBody = "Metrics on this tab represent all transactions including cash, based on the stores selling the product within the Catalina network. No shopper level statics are applied. Sales: Total dollar sales of the brand before discounts have been appliedUnits: Also referred to as \"unit movement,\" this metric represents the sum of total products soldMSelling Stores: Distinct count of physical stores with sales of the focus brandDollar Share: The brand share of dollars spent in the entire categoryTrip Incidence: # of brand trips divided by # of category trips. A percent indication of how often a category trip includes a specified brand.Sales Per Trip: Average dollars spent on the brand (Brand Sales / Brand Trips) in a single transaction.Units Per Trip: Average number of units purchased in a single transaction (Brand Units / Brand Trips)Sales Per Store: Average sales per point of distribution (Brand Dollars/Selling Stores of the Brand)Units Per Store: Average number of units purchased per point of distribution (Units/ Selling Stores of the brand)";
}
