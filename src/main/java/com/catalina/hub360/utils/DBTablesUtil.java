package com.catalina.hub360.utils;

import java.io.IOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.util.List;


/**
 * This class holds methods to query 
 * @author srajeshw
 *
 */
public class DBTablesUtil {

	String dbUrl ="jdbc:mysql://orlucfdbv01.catmktg.com:3306/optimusprime";
	String	dbusername = "qauser";
	String	dbpassword = "q@us3r";
	String jdbcDriver = "com.mysql.jdbc.Driver";

	/**
	 * 
	 * @param offercode
	 * @param agent
	 * @return
	 * @throws IOException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public List<String> cmsHandRaiserOfferPeriodTable(String offerId, String agent) throws IOException,IOException, ClassNotFoundException, SQLException {




		List<String> slist = new ArrayList<String>();

		String query;

		try{  

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			//Execution of Query as per requirement

			query ="select * from cms.hand_raiser_offer_period where agent like '%"+agent+"%' and offer_id="+offerId+";";

			ResultSet rs=stmt.executeQuery(query);
			
			System.out.println(query);

			while (rs.next()) {

				String sdr=rs.getString("start_date").concat(" - ").concat(rs.getString("end_date"));

				
				slist.add(new String(sdr)); 


			}

			//Closing DB connection
			con.close();
			rs.close();
			return slist;

		}catch (SQLException e){

			System.out.println("Database Error");

			e.printStackTrace();

			return slist;

		}

	}



	/* Method			    : cmsHandRaiser_Date_HistoryTablespecific(String offercode, String agent)
	   Return Value			: List[String]
	   Method Description	: Method to get startdate and Enddate from database- to verify the history of dates - mysql 
	 */
	/**
	 * 
	 * @param offercode
	 * @param agent
	 * @return
	 * @throws IOException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public List<String> cmsHandRaiserDateHistoryTablespecific(String offercode, String agent) throws IOException,IOException, ClassNotFoundException, SQLException {
		
		List<String> slist = new ArrayList<String>();

		String query;

		try{  

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			//Execution of Query as per requirement
			query ="SELECT * FROM cms.hand_raiser_date_history  where hand_raiser_offer_period_id in"+
					" "+"(select id from cms.hand_raiser_offer_period where agent like '%"+agent+"%' and offer_id="+offercode+");";


			ResultSet rs=stmt.executeQuery(query);
			System.out.println(query);

			while (rs.next()) {
			
//				System.out.println("Start Date ::"+rs.getString("start_date"));
//				System.out.println("End Date ::"+rs.getString("end_date"));
				String sdr=rs.getString("start_date").concat(" - ").concat(rs.getString("end_date"));

//				slist.add(new String(rs.getString("start_date"))); 
				slist.add(new String(sdr)); 
				//System.out.println(slist);

			}

			//Closing DB connection
			con.close();

			rs.close();
			return slist;

		}catch (SQLException e){

			System.out.println("Database Error");

			e.printStackTrace();

			return slist;

		}

	}





	/* Method			    : cmsOfferDataValidation(String status, String agent, String offerid)
	   Return Value			: List[String]
	   Method Description	: Method to get startdate and enddate from datbase - mysql 
	 */

	/**
	 * 
	 * @param status
	 * @param agent
	 * @param offerid
	 * @return
	 * @throws IOException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public List<String> cmsOfferDataValidation(String offerid,String enabled, String extraname) throws IOException,IOException, ClassNotFoundException, SQLException {


		List<String> slist = new ArrayList<String>();
		String query;


		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			//Execution of Query as per requirement
			query ="select h.offer_id,h.agent,a.display_name, ex.extra_value, o.title, o.face_value, h.start_date, h.end_date, h.reason, h.status" + 
					" "+"from cms.offer o" + 
					" "+"join cms.hand_raiser_offer_period h on o.id=h.offer_id" + 
					" "+"join cms.offer_extra_param ex on o.id=ex.offer_id" + 
					" "+"join cms.agent a on a.agent=h.agent" + 
					" "+"where o.is_enabled="+enabled+"  and o.CPG_OFFER_CODE="+offerid+" and ex.extra_name = '"+extraname+"' and a.display_name is not NULL order by a.display_name ;";

			
			//and h.status='"+status+"' and h.agent='"+agent+"'String status, String agent, 

			//ResultSet rs = stmt.executeQuery("use optimusprime;");

			ResultSet rs=stmt.executeQuery(query);

			while (rs.next()) {

				 


				slist.add(rs.getString("start_date"));
				slist.add(rs.getString("end_date"));
				slist.add(rs.getString("status"));
			
				
			}
			//Closing DB connection
			con.close();

			rs.close();

		}catch (SQLException e){

			System.out.println("Database Error");

			e.printStackTrace();



		}
		return slist;

	}
	
	/**
	 * @param offerCode
	 * @return
	 * @throws IOException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public String getOfferIdbyPassingOfferCode(String offerCode, String title) throws IOException,IOException, ClassNotFoundException, SQLException {


		String offerId = null;
		String query;


		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			//Execution of Query as per requirement
			query ="Select * from cms.offer where CPG_OFFER_CODE="+offerCode+" and TITLE like'%"+title+"%';";

			ResultSet rs=stmt.executeQuery(query);
			System.out.println(query);

			while (rs.next()) {

				offerId=rs.getString("ID");
			}
			//Closing DB connection
			con.close();

			rs.close();

		}catch (SQLException e){

			System.out.println("Database Error");

			e.printStackTrace();



		}
		return offerId;

	}
	
	
	
	
	
	
	
	public void deleteOfferByCem() throws IOException,IOException, ClassNotFoundException, SQLException {


	
		String query;


		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			//Execution of Query as per requirement
			query ="delete from cms.offer_by_cem where offer_period_id in (select id from cms.offer_period where offer_id in (select distinct(hr.offer_id) from cms.hand_raiser_offer_period hr join cms.offer_owner ow on ow.offer_id = hr.offer_id" + 
					" "+"where hr.status in ('Hr Rejected','Hr Approved') and ow.owner = 'General Mills' and hr.requester like '%cs auto%' and agent like '%AFS%'));";

			ResultSet rs=stmt.executeQuery(query);
			System.out.println(query);
			System.out.println("Deletion is Successfull");
			
			//Closing DB connection
			con.close();

			rs.close();

		}catch (SQLException e){

			System.out.println("Database Error");

			e.printStackTrace();



		}
		

	}
	
	
	public void deleteOfferCmsOfferOvveride() throws IOException,IOException, ClassNotFoundException, SQLException {


		
		String query;


		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			//Execution of Query as per requirement
			query ="delete from cms.offer_override where offer_id in (select distinct(hr.offer_id) from cms.hand_raiser_offer_period hr join cms.offer_owner ow on ow.offer_id = hr.offer_id" + 
					" "+"where hr.status in ('Hr Rejected','Hr Approved') and ow.owner = 'General Mills'and hr.requester like '%cs auto%' and agent like '%AFS%');";

			ResultSet rs=stmt.executeQuery(query);
			System.out.println(query);
			System.out.println("Deletion is Successfull");
			
			//Closing DB connection
			con.close();

			rs.close();

		}catch (SQLException e){

			System.out.println("Database Error");

			e.printStackTrace();



		}
		

	}
	
	
public void deleteOfferCms() throws IOException,IOException, ClassNotFoundException, SQLException {


		
		String query;


		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			//Execution of Query as per requirement
			query ="delete from cms.offer where id in (select distinct(hr.offer_id) from cms.hand_raiser_offer_period hr join cms.offer_owner ow on ow.offer_id = hr.offer_id" + 
					" "+"where hr.status in ('Hr Rejected','Hr Approved') and ow.owner = 'General Mills'and hr.requester like '%cs auto%' and agent like '%AFS%');" ;

			ResultSet rs=stmt.executeQuery(query);
			System.out.println(query);
			System.out.println("Deletion is Successfull");
			
			//Closing DB connection
			con.close();

			rs.close();

		}catch (SQLException e){

			System.out.println("Database Error");

			e.printStackTrace();



		}
		

	}
	
	
	

	/**
	 * 
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
private Connection getConnectionStatement() throws IOException, ClassNotFoundException, SQLException {

	//Connecting to DB 
	Class.forName(jdbcDriver);
	return DriverManager.getConnection(dbUrl, dbusername, dbpassword);

}

/**
 * @param campaignName
 * @return
 * @throws IOException
 * @throws IOException
 * @throws ClassNotFoundException
 * @throws SQLException
 */
public List<String> campaignVerification(String campaignName) throws IOException,IOException, ClassNotFoundException, SQLException {


	List<String> slist = new ArrayList<String>();
	String query;


	try{   

		Connection con = getConnectionStatement();
		Statement stmt = con.createStatement();

		//Execution of Query as per requirement
		query ="select * from cms.offer where CAMPAIGN='"+campaignName+"';";

		System.out.println(query);

		ResultSet rs=stmt.executeQuery(query);

		while (rs.next()) {

			 


			slist.add(rs.getString("TITLE"));
			
		
			
		}
		//Closing DB connection
		con.close();

		rs.close();

	}catch (SQLException e){

		System.out.println("Database Error");

		e.printStackTrace();



	}
	return slist;

}
}

