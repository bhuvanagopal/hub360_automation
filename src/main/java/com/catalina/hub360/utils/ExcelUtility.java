package com.catalina.hub360.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import au.com.bytecode.opencsv.CSVReader;


public class ExcelUtility
{
	 DataFormatter dataFormatter = new DataFormatter();
	  String cellValue;
	  String imageURL;
	  
	 public void csvReader(String filename)throws Exception {
	        try {
	        	
	        	CSVReader csvReader = new CSVReader(new FileReader(filename));
	        	String[] row = null;
	        	
	        	while((row = csvReader.readNext()) != null) {
	        		List<String> list = new ArrayList<String>();
	        		for(int i=0; i<row.length; i++)
	        		{
	        		list.add(row[i]);
	        		}
	        		System.out.println(list);
	        	}
	        	csvReader.close();
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
	 
	 public String imageURLreader(String filename) throws Exception{
		 
		 try {
	            Workbook wb = WorkbookFactory.create(new File(filename));
	            Sheet datatypeSheet = wb.getSheetAt(0);
	            Iterator<Row> iterator = datatypeSheet.iterator();

	            while (iterator.hasNext()) {
	                	 imageURL=  wb.getSheetAt(0).getRow(1).getCell(CellReference.convertColStringToIndex("k")).toString();
	     	            System.out.println("ImageURL::  "+imageURL);
	     	            break;
	            }
	     	        } catch (FileNotFoundException e) {
	     	            e.printStackTrace();
	     	        } catch (IOException e) {
	     	            e.printStackTrace();
	     	        }
	     			return imageURL;
	 }
	 
	 
	 public boolean checkDate(String file, String operatingUnit, String startDate, String endDate) throws IOException {
			FileUtil utils = new FileUtil();
			boolean found = false;
			ArrayList<ArrayList<String>> list = utils.excelReaderExport(file, 0, 9);
			for (List<String> row : list)
				if (row.get(1).equalsIgnoreCase(operatingUnit) && row.get(5).equalsIgnoreCase(startDate)
						&& row.get(6).equalsIgnoreCase(endDate))
					found = true;
			return found;
		}
	}