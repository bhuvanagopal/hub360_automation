package com.catalina.hub360.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;
import org.unitils.thirdparty.org.apache.commons.io.FileUtils;

/**
 * 
 * @author automation
 *
 */
public class FileUtil {

	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */
	public List<File> getFilesInFolder(String folderPath,
			String fileNamePattern) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);

		File files[] = folder.listFiles();
		if (files == null)
			return list;
		for (File file : files) {
			if (file.getName().contains(fileNamePattern))
				list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */
	public List<File> getFilesInFolder(String folderPath,
			String fileNamePattern, String fileExtension) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return list;
		for (File file : files) {
			if (file.getName().contains(fileNamePattern)
					&& file.getName().contains(fileExtension))
				list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param folderPath
	 * @param fileNamePattern
	 */

	/**
	 * 
	 * @param folderPath
	 */
	public List<File> getFilesInFolder(String folderPath) {
		List<File> list = new ArrayList<>();
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return list;
		for (File file : files) {
			list.add(file);
		}
		return list;
	}

	/**
	 * 
	 * @param sourceFolderPath
	 * @param targetFolderPath
	 */
	public boolean moveFilesToFolder(String sourceFolderPath,
			String targetFolderPath) {
		File folder = new File(sourceFolderPath);
		boolean flag = true;
		File files[] = folder.listFiles();
		if (files == null)
			return false;
		for (File file : files) {
			if (file.renameTo(new File(targetFolderPath + file.getName()))) {
				file.delete();
			} else {
				flag = false;
			}
		}
		return flag;
	}

	/**
	 * 
	 * @param sourceFolderPath
	 * @param targetFolderPath
	 * @param fileName
	 */
	public boolean moveFilesToFolder(String sourceFolderPath,
			String targetFolderPath, String fileName) {
		File folder = new File(sourceFolderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return false;
		for (File file : files) {
			if (file.getName().contains(fileName))
				if (file.renameTo(new File(targetFolderPath + file.getName()))) {
					file.delete();
					return true;
				}
		}
		return false;
	}

	/**
	 * 
	 * @param folderPath
	 * @return
	 */
	public boolean deleteFilesInFolder(String folderPath) {
		File folder = new File(folderPath);
		File files[] = folder.listFiles();
		if (files == null)
			return false;
		try {
			for (File file : files)
				file.delete();
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public String fileTOString(File file) throws IOException {
		StringBuilder str = new StringBuilder();
		FileInputStream fin = new FileInputStream(file);
		int i = 0;
		while ((i = fin.read()) != -1) {
			str.append((char) i);
		}
		fin.close();
		fin = null;
		return str.toString();
	}

	/**
	 * This metjod returns the windows equivalent of the linux shared rive path
	 * 
	 * @param linuxPath
	 * @param windowsSharedDrive
	 * @return
	 */
	public String getWindowsPath(String linuxPath,
			String windowsSharedDrive) {
		String tokens[] = linuxPath.split("/");

		StringBuffer buf = new StringBuffer();
		for (String token : tokens)
			if (!token.trim().isEmpty())
				buf.append("\\").append(token);

		return "\\\\" + windowsSharedDrive + buf;
	}

	/**
	 * @param destinationPath
	 * @param fileName
	 * @param file
	 */
	public void copyFile(String destinationPath, String fileName,
			File file) throws IOException {
		FileUtils.copyFile(file, new File(destinationPath + fileName));
	}

	public String getFileByPattern(String path, String pattern) {
		// TODO Auto-generated method stub
		Pattern pattern1 = Pattern.compile(pattern.replace("\\", "").replace("*", "[a-zA-Z0-9]*"));
		Matcher matcher = null;
		File folder = new File(path);
		String name =null;
		for(File file : folder.listFiles()){
			matcher = pattern1.matcher(file.getName());
			if(matcher.lookingAt())
				name = file.getName().substring(0, file.getName().lastIndexOf("."));
		}
		return "\\"+name;
	}
	
	
	// #############################Read from
		// Excel###################################################################
		public ArrayList<ArrayList<String>> excelReader(String file, int sheetNo, int noOfColumns) throws IOException {
			ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
			File file1 = new File(file);
			FileInputStream fis = new FileInputStream(file1);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheetAt(sheetNo);

			Iterator<Row> rows = sheet.iterator();

			while (rows.hasNext()) {
				XSSFRow row = (XSSFRow) rows.next();
				ArrayList<String> column = new ArrayList<String>();
				for (int i = 0; i <= noOfColumns; i++) {
					DataFormatter formatter = new DataFormatter();
					column.add(formatter.formatCellValue(row.getCell(i)));
				}
				list.add(column);
			}

			return list;
		}
		
		public ArrayList<ArrayList<String>> excelReaderExport(String file, int sheetNo, int noOfColumns) throws IOException {
			ArrayList<ArrayList<String>> list = new ArrayList<ArrayList<String>>();
			File file1 = new File(file);
			FileInputStream fis = new FileInputStream(file1);
			HSSFWorkbook workbook = new HSSFWorkbook(fis);
			HSSFSheet sheet = workbook.getSheetAt(sheetNo);

			Iterator<Row> rows = sheet.iterator();

			while (rows.hasNext()) {
				HSSFRow row = (HSSFRow) rows.next();
				ArrayList<String> column = new ArrayList<String>();
				for (int i = 0; i <= noOfColumns; i++) {
					DataFormatter formatter = new DataFormatter();
					column.add(formatter.formatCellValue(row.getCell(i)));
				}
				list.add(column);
			}

			return list;
		}

		public void appendStrToFile(File fileName, String str) 
		{ 
		try { 

		// Open given file in append mode. 
		BufferedWriter out = new BufferedWriter( 
		new FileWriter(fileName, true)); 
		out.write(str); 
		out.append('\n');
		out.close(); 
		} 
		catch (IOException e) { 
		System.out.println("exception occoured" + e); 
		} 
		} 
		
		public void colorValidation(WebElement element,String colorExpected)
		{
			String color = element.getCssValue("color");
			String hex = Color.fromString(color).asHex();
			System.out.println(hex);
			Assert.assertEquals(hex, colorExpected);
		}
}
