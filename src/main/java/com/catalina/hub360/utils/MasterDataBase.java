package com.catalina.hub360.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class MasterDataBase {

	WebDriver driver;
	String dbUrl ="jdbc:mysql://orlucfdbv01.catmktg.com:3306/optimusprime";
	String	dbusername = "qauser";
	String	dbpassword = "q@us3r";
	String jdbcDriver = "com.mysql.jdbc.Driver";

	String campaignNamedb = null;
	String campaignName;
	String campaignStatus;
	
	private Connection getConnectionStatement() throws IOException, ClassNotFoundException, SQLException {

		//Connecting to DB 
		Class.forName(jdbcDriver);
		return DriverManager.getConnection(dbUrl, dbusername, dbpassword);

	}


	/**
	 * @return
	 * @throws Throwable
	 */
	public String verifyTheCampaignNameSaved() throws Throwable {

		
		String query;

		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			query = "select * from conweb.a2o_campaigns ORDER BY id desc limit 1;";

			ResultSet rs=stmt.executeQuery(query);  
			while (rs.next()) {

				System.out.println("campaignName ::"+rs.getString("name"));
				campaignNamedb =rs.getString("name");
				campaignName = this.campaignNamedb;
			}

		}catch (SQLException e){

			System.out.println("Database Error");
			e.printStackTrace();

		}
		return campaignNamedb;
	}
	
public String verifyTheStatusOfCampaign() throws Throwable {

		
		String query;

		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			query = "select * from conweb.a2o_campaigns ORDER BY id desc limit 1;";

			ResultSet rs=stmt.executeQuery(query);  
			while (rs.next()) {

				System.out.println("campaignName ::"+rs.getString("name"));
				campaignStatus =rs.getString("status");
			}

		}catch (SQLException e){

			System.out.println("Database Error");
			e.printStackTrace();

		}
		return campaignStatus;
	}
	
	public void deleteCampaign() throws Throwable {

		String query;

		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			query = "delete from conweb.a2o_campaigns where name like '%Automation%';";
			
			System.out.println(query);
			
			stmt.executeUpdate(query);
			
			System.out.println("Deleted campaign:"+campaignName);

		}catch (SQLException e){

			System.out.println("Database Error");
			e.printStackTrace();

		}
		
	}
	public List<String> adminPageDetail() throws IOException,IOException,ClassNotFoundException, SQLException {
		List<String> emailList = new ArrayList<String>();
		String query;

		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			query = "select t1.email from elportal.user t1,elportal.user_partner t2, elportal.partner t3 where (t1.roles like '%coupon%' or t1.roles like '%retailer%')  and t2.user_id=t1.user_id and t2.partner_id=t2.partner_id order by t3.name, t1.active desc ,t1.last_name,t1.first_name;";

			ResultSet rs=stmt.executeQuery(query);  

			while (rs.next()) {
				emailList.add(new String(rs.getString("email"))); 
			}


		}catch(Exception e) {
			System.out.println("unable to fetch admin page data from database");
		}
		return emailList;
	}
	public void verifyClonnedCampaign(String campaignName) throws IOException,IOException,ClassNotFoundException, SQLException {
		
		String query;
		Boolean campaign=false;
		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			query = "select * from conweb.a2o_campaigns where name = '"+campaignName+"';";

			ResultSet rs=stmt.executeQuery(query);  

			while (rs.next()) {
				campaign=true;
			}


		}catch(Exception e) {
			System.out.println("unable to fetch admin page data from database");
		}
		 Assert.assertEquals(true,  campaign);
	}
	
	public ArrayList<String> returnHiddenRetailarName() {
		String query;
		
		
		ArrayList<String> hiddenRetailerNameList = new ArrayList<String>();
		
		
		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			query = "select m.id,m.name,a.ID as agent_id,a.display_name as agent,m.legal_name,a.supports_banner_targeting as bt,a.ID as agent_id,m.is_hidden,m.is_enabled from cms.merchant m,cms.agent a where m.merchant_category in ('Local', 'National') and m.is_cem = true and m.agent = a.agent and (m.is_hidden = 1 and m.is_enabled = 1) and date(m.expire_on) > now() order by m.agent;";

			ResultSet rs=stmt.executeQuery(query);  

			while (rs.next()) {
				hiddenRetailerNameList.add(new String(rs.getString("legal_name")));
			}


		}catch(Exception e) {
			System.out.println("unable to fetch admin page data from database");
		}
	
		return hiddenRetailerNameList;
	}
	
	public ArrayList<String> returnUnHiddenRetailarName() {
		String query;
		
		
		ArrayList<String> hiddenRetailerNameList = new ArrayList<String>();
		
		
		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			query = "select m.id,m.name,a.ID as agent_id,a.display_name as agent,m.legal_name,a.supports_banner_targeting as bt,a.ID as agent_id,m.is_hidden,m.is_enabled from cms.merchant m,cms.agent a where m.merchant_category in ('Local', 'National') and m.is_cem = true and m.agent = a.agent and (m.is_hidden = 0 and m.is_enabled = 1) and date(m.expire_on) > now() order by m.agent;";

			ResultSet rs=stmt.executeQuery(query);  

			while (rs.next()) {
				hiddenRetailerNameList.add(new String(rs.getString("legal_name")));
			}


		}catch(Exception e) {
			System.out.println("unable to fetch admin page data from database");
		}
	
		return hiddenRetailerNameList;
	}
	
public void verifyHistoryChangeOfCampaign(String campaignId) throws IOException,IOException,ClassNotFoundException, SQLException {
		
		String query;
		Boolean campaign=false;
		int count = 0;
		ArrayList<String> list = new ArrayList<String>();
		try{   

			Connection con = getConnectionStatement();
			Statement stmt = con.createStatement();

			query = "select * from conweb.a2o_campaigns_changelogs where campaign_id = '"+campaignId+"';";

			ResultSet rs=stmt.executeQuery(query);  

			while (rs.next()) {
				list.add(new String(rs.getString("changes"))); 
				count = list.size();
				
				campaign=true;
			}

			Assert.assertEquals(count, 7);
		}catch(Exception e) {
			System.out.println("unable to fetch logs from database");
		}
		 Assert.assertEquals(true,  campaign);
	}

public void verifyTheRevTraxDataOfCampaign(String campaignId,String merchantRevTrax, String affiliateRevTrax,String programRevTrax) throws Throwable {

	String merchant = "";
	String affiliate= "";
	String program= "";
	String query;

	try{   

		Connection con = getConnectionStatement();
		Statement stmt = con.createStatement();

		query = "select * from conweb.a2o_coupons where campaign_id = '"+campaignId+"';";

		ResultSet rs=stmt.executeQuery(query);  
       
		while (rs.next()) {
			merchant=rs.getString("merchant");
			affiliate=rs.getString("affiliate");
			program = rs.getString("program");
		}

	}catch (SQLException e){

		System.out.println("Database Error");
		e.printStackTrace();

	}
	 Assert.assertEquals(merchant,  merchantRevTrax);
	 Assert.assertEquals(affiliate,  affiliateRevTrax);
	 Assert.assertEquals(program,  programRevTrax);
}
}
