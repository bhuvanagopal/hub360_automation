package com.catalina.hub360.utils;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



/**
 * @author avanish
 *
 */
/**
 * @author santoshs
 *
 */
public class ObjectActions {
	String downLoadedFileName;
	 private static ObjectActions singletonObj;
	 private WebDriver driver; 
	    /**
	     * Create private constructor
	     */
	    private ObjectActions(WebDriver driver){
	         this.driver=driver;
	    }
	    /**
	     * Create a static method to get instance.
	     */
	    public static ObjectActions getInstance(WebDriver driver){
	        if(singletonObj == null){
	        	singletonObj = new ObjectActions(driver);
	        }
	        return singletonObj;
	    }
	    
	    
	/**
	 * Selects the drop down value by visible text
	 * 
	 * @param element	: the web element which is expected to be of select type
	 * @param text		: the visible text present in the select drop down
	 * @return true		: if the given text is selected the return value is true else it returns false
	 * @throws			: IOException
	 */
	public boolean selectDropdownByVisibleText(WebElement element, String text) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed() && element.isEnabled()) {
						// ViewElement(element);
						Select elementSelectObj = new Select(element);
						if (!(elementSelectObj.getOptions().size() > 1))
							driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS) ;
							List<WebElement> AllOptions = elementSelectObj.getOptions();
							for (WebElement itr : AllOptions) {
							// System.out.println(itr.getText());
								if (itr.getText().trim().equalsIgnoreCase(text.trim())) {
								System.out.println("Executed: selectDropdownByVisibleText, PASS");
								itr.click();
								return true;
								}
							}

						return false;
						}
					else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}

		return false;
	}
	
	
	/**
	 * Selects the drop down value by visible text
	 * 
	 * @param element	: the web element which is expected to be of select type
	 * @param text		: the visible text present in the select drop down
	 * @return true		: if the given text is selected the return value is true else it returns false
	 * @throws			: IOException
	 */
	public boolean selectDropdownByIndex(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed() && element.isEnabled()) {
						// ViewElement(element);
						Select elementSelectObj = new Select(element);
						elementSelectObj.selectByIndex(1);
								return true;
								}
							
					
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}

		return false;
	}
	    
/******************************************************************************
* Method Name			: clickElement
* Description			: clicks on the web element
* @param element		: web element on which the click has to be performed
* @return				: if the element is visible on screen it will be clicked and  returns true
* @throws				: IOException
*/
	public boolean clickElement(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {

				try {
					// (new
					// WebDriverWait(driver,5)).until(ExpectedConditions.visibilityOf(element));
					// ViewElement(element);
					if (element.isDisplayed()) {
						element.click();
						System.out.println("Executed :clickElement, PASS");
						return true;
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
/************************************************************************************
* Method Name		: verifyElementEnabled(WebElement element) 	
* Description		: It verifies if the web element is displayed and is enabled
* @param element	: web element which is to verified for enabled 
* @return 			: returns true if web element is displayed and enabled
* @throws 			: IOException
*/
	public boolean verifyElementEnabled(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					// ViewElement(element);
					if (element.isDisplayed() && element.isEnabled()) {

						System.out.println("Executed :verifyElementEnabled, PASS");
						return true;
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
		
		
/*****************************************************************************************
* Description		: It verifies if the web element is displayed and is disabled
* @param element	: web element which is to verified for disabled 
* @return 			: returns true if web element is displayed and disabled
* @throws			: IOException
*/
	public boolean verifyElementDisabled(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					// ViewElement(element);
					if (element.isDisplayed() && !(element.isEnabled())) {

						System.out.println("Executed :verifyElementDisabled, PASS");
						return true;
					} else if (element.isDisplayed() && element.getAttribute("readonly") != null
							&& element.getAttribute("readonly").trim().equalsIgnoreCase(String.valueOf(true))) {
						System.out.println("Executed :verifyElementDisabled, PASS");
						return true;
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
		
		
/******************************************************************************************
* Description		: brings focus to on the web element and drags it on top of the page by scrolling
* @param			: element element which has to be viewed and dragged on top of the page
* @return			: returns true if element is displayed
* @throws			: IOException
* @throws			: InterruptedException
*/
	public boolean ViewElement(WebElement element) throws IOException, InterruptedException {

		try {
			((JavascriptExecutor) driver).executeScript("window.focus();");
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS) ;
			return true;
		} catch (Throwable e) {
			return false;
		}
	}
		
		
/**********************************************************************************
* Description		: Compares the value attribute of the web element with the given text and return true if the text matches the value
* @param			: element element whose value attribute has to be matched 
* @param			: text the text for comparison with web element value attribute
* @return			: returns true if the web element is displayed and the value attribute matches with given text
* @throws			: IOException
*/
	public boolean verifyWebElementValueAttribute(WebElement element, String text) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						if (element.getAttribute("value").trim().equals(text.trim())) {
							System.out.println("Executed :verifyWebElementValueAttribute, PASS");
							return true;
						}
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
		
		/**
		 * Compares the value attribute of the webelement with the given text and return true if the text is contained in the value
		 * @param element element whose value attribute has to be matched 
		 * @param text the text for comparison with webelement value attribute
		 * @return returns true if the webelement is displayed and the value attribute matches with given text
		 * @throws IOException
		 */
	public boolean verifyWebElementValueAttribute_contains(WebElement element, String text) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						if (element.getAttribute("value").contains(text.trim())) {
							System.out.println("Executed :verifyWebElementValueAttribute, PASS");
							return true;
						}
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
		/**
		 * Verifies for invisibility of the given webelement
		 * @param element the webelement which is exepected to not to be visible
		 * @return returns true if the webelement is not visible on the screen or it does not exists
		 * @throws IOException
		 */
	public boolean verifyElementNotDisplayed(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {

					if (!element.isDisplayed()) {
						System.out.println("Executed :verifyElementNotDisplayed, PASS");
						return true;
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (NoSuchElementException e) {
					return true;
				}
			}
			if (!element.isDisplayed())
				return false;

		} catch (Exception e) {
			return false;
		}
		return true;
	}
		/**Verifies that the webelement is visible on the screen
		 * @param element  the webelement which is exepected to be visible 
		 * @return true if element is displayed 
		 * @throws IOException
		 */
	public boolean verifyElementDisplayed(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						System.out.println("Executed :verifyElementDisplayed, PASS");
						return true;
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**Verifies that the webelement is visible on the screen
	 * @param element  the webelement which is exepected to be visible 
	 * @return true if element is displayed 
	 * @throws IOException
	 */
public boolean verifyElementDisplayed1(WebElement element) throws IOException {
	int i = 0;
	try {
		for (i = 0; i < 2; i++) {
			try {
				(new WebDriverWait(driver, 3)).until(ExpectedConditions.visibilityOf(element));
				if (element.isDisplayed()) {
					// ViewElement(element);
					System.out.println("Executed :verifyElementDisplayed, PASS");
					return true;
				} else {
					System.out.println("Trying to find object");
					continue;
				}
			} catch (TimeoutException | NoSuchElementException e) {
				System.out.println("Trying to find object");
				continue;
			}
		}
	} catch (Exception e) {
		return false;
	}
	return false;
}

public boolean verifyListElementDisplayed(List<WebElement> element) throws IOException {
	int i = 0;
	try {
		for (i = 0; i < 2; i++) {
			try {
				(new WebDriverWait(driver, 3)).until(ExpectedConditions.visibilityOf((WebElement) element));
				if (((WebElement) element).isDisplayed()) {
					// ViewElement(element);
					System.out.println("Executed :verifyElementDisplayed, PASS");
					return true;
				} else {
					System.out.println("Trying to find object");
					continue;
				}
			} catch (TimeoutException | NoSuchElementException e) {
				System.out.println("Trying to find object");
				continue;
			}
		}
	} catch (Exception e) {
		return false;
	}
	return false;
}
	
	
/**	Description		: Clears the text of the web element and enters the given text into it
* 	@param			: element the web element which is expected to be passed the value
* 	@param			: text the value to be passed to the web element
*	@return			: true if the web elemnt is displayed, enabled and the value is entered successfully into the webelement
* 	@throws			: IOException
*/
	public boolean sendText(WebElement element, String text) throws IOException {
		int i = 0;
		try {	
			for (i = 0; i < 7; i++) {
				try {	
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed() && element.isEnabled()) {
						// ViewElement(element);					
						element.clear();
						element.sendKeys(text);
						driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
						if (!element.getAttribute("value").contains(text.trim())) {
							driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
							System.out.println("Trying to find object");
							continue;
						}
						System.out.println("Executed :sendText, PASS");
						return true;
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
		
		/**returns the text contained inside the webelement
		 * @param element the element whose text is required to be extracted
		 * @return the text contained in the webelement
		 * @throws IOException
		 */
	public String getWebElementText(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed() && element.isEnabled()) {
						// ViewElement(element);
						return element.getText();
						
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	/**returns the text contained inside the webelement
	 * @param element the element whose text is required to be extracted
	 * @return the text contained in the webelement
	 * @throws IOException
	 */
public String getWebElementText1(WebElement element) throws IOException {
	int i = 0;
	try {
		for (i = 0; i < 7; i++) {
			try {
				(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
				if (element.isDisplayed()) {
					// ViewElement(element);
					return element.getText();
					
				} else {
					System.out.println("Trying to find object");
					continue;
				}
			} catch (TimeoutException | NoSuchElementException e) {
				System.out.println("Trying to find object");
				continue;
			}
		}
	} catch (Exception e) {
		return null;
	}
	return null;
}
	
	/**returns the text contained inside the webelement
	 * @param element the element whose text is required to be extracted
	 * @return the text contained in the webelement
	 * @throws IOException
	 */
public String getListWebElementText(List<WebElement> element) throws IOException {
	int i = 0;
	try {
		for (i = 0; i < 7; i++) {
			try {
				(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf((WebElement) element));
				if (((WebElement) element).isDisplayed() && ((WebElement) element).isEnabled()) {
					// ViewElement(element);
					return ((WebElement) element).getText();
					
				} else {
					System.out.println("Trying to find object");
					continue;
				}
			} catch (TimeoutException | NoSuchElementException e) {
				System.out.println("Trying to find object");
				continue;
			}
		}
	} catch (Exception e) {
		return null;
	}
	return null;
}
		
		/**returns the text contained inside the webelement
		 * @param element the element whose text is required to be extracted
		 * @return the text contained in the webelement
		 * @throws IOException
		 */
	public String getWebElementValue(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						return element.getAttribute("value").trim();
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return null;
		}

		return null;
	}

/**********************************************************************************
	* Method Name		: clickElementByVisibleText(String text)
	* Description		: Compares the value attribute of the web element with the given text and return true if the text matches the value
	* @param			: element element whose value attribute has to be matched 
	* @param			: text the text for comparison with web element value attribute
	* @return			: returns true if the web element is displayed and the value attribute matches with given text
	* @throws			: IOException
*/
	
	public boolean clickElementByVisibleText(String text) throws IOException {

		int i = 0;
		try

		{
			WebElement element = driver.findElement(By.xpath("(//*[contains(text(),'" + text + "')])[1]"));
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed() && element.isEnabled()) {
						// ViewElement(element);
						element.click();
						System.out.println("Executed :clickElementByVisibleText, PASS");
						return true;
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

		/** Verifies whether the given text matches the text contained in webelement
		 * @param element
		 * @param text
		 * @return returns true if the given text matches the webelement's text
		 * @throws IOException
		 */
	public boolean verifyWebElementText(WebElement element, String text) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						if (element.getText().trim().equalsIgnoreCase(text.trim())) {
							System.out.println("Executed :verifyWebElementText, PASS");
							return true;
						}
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
		
		/** Verifies whether the given text matches the text contained in webelement
		 * @param element
		 * @param text
		 * @return returns true if the given text matches the webelement's text
		 * @throws IOException
		 */
	public boolean verifyWebElementText_contains(WebElement element, String text) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						if (element.getText().trim().toUpperCase().contains(text.trim().toUpperCase())) {
							System.out.println("Executed :verifyWebElementText, PASS");
							return true;
						}
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
		/**Verifies whether the select webelement's selected value matches with given text
		 * @param element
		 * @param text
		 * @return returns true if the selected option text matches with given text as a parameter
		 * @throws IOException
		 */
	public boolean verifySelectListSelectedValue(WebElement element, String text) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						Select elementSelectObj = new Select(element);
						String selectedText = elementSelectObj.getFirstSelectedOption().getText();
						if (selectedText.trim().equalsIgnoreCase(text.trim()))
							return true;
						else
							return false;

					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
		
		
		
		/**Returns the number of files in the given directory
		 * @param Dir absolute  path of the directory 
		 * @param ext the  extension of the files to be searched for, e.g. for excel file-"xls"
		 * @return returns the number of files present in directory with the given extension
		 */
	public int getFilesInDirWithExt(String Dir, String ext) {
		try {
			ext = "*." + ext;
			LinkedList<String> files = new LinkedList();
			files = (LinkedList<String>) getAllFilesThatMatchFilenameExtension(Dir, ext);
			System.out.println(files.size());
			return files.size();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
		/**Returns the list of files in the given directory
		 * @param Dir absolute  path of the directory 
		 * @param ext the  extension of the files to be searched for, e.g. for excel file-"*.xls"
		 * @return returns the list of files present in directory with the given extension
		 */
	Collection getAllFilesThatMatchFilenameExtension(String directoryName, String extension) {

		try {
			File directory = new File(directoryName);
			return FileUtils.listFiles(directory, new WildcardFileFilter(extension), null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		}
		 
		 /** returns the text after  eliminating all the characters from the given text except digits
		 * @param text
		 * @return
		 * @throws IOException
		 * @throws InterruptedException
		 */
	public String removeContentExceptNumber(String text) {
		text = text.replaceAll("[^0-9]", "").replaceAll(" ", "");
		return text;
	}
	
		 /** verifies if any child object of the given webelement with the given xpath exists and displayed
		 * @param element element which is expected to contain the child object with given xpath
		 * @param childXpath xpath of the child object
		 * @return
		 * @throws IOException
		 */
	public boolean verifyChildObjectByXpathDisplayed(WebElement element, String childXpath) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						element.findElement(By.xpath(childXpath));
						if (element.findElement(By.xpath(childXpath)).isDisplayed())
							return true;
						else
							return false;
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
		
		/** Check the given webelement checkbox if it is of type checkbox
		 * @param element the webelement of type checkbox
		 * @return true if the webelement is displayed, enabled and of checkbox type 
		 * @throws IOException
		 */
	public boolean unCheckCheckBox(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed() && element.isEnabled()) {
						// ViewElement(element);
						if (element.getAttribute("type").equalsIgnoreCase("checkbox")) {
							if (!element.isSelected())
								element.click();
							driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
							element.click();
							System.out.println("Executed :unCheckCheckBox, PASS");
							return true;
						} else {
							return false;
						}
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	 /** Check the given webelement checkbox if it is of type checkbox
	 * @param element the webelement of type checkbox
	 * @return true if the webelement is displayed, enabled and of checkbox type 
	 * @throws IOException
	 */
	public boolean checkCheckBox(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					Thread.sleep(3000);
					if (element.isDisplayed() && element.isEnabled()) {
						// ViewElement(element);
						if (element.getAttribute("type").equalsIgnoreCase("checkbox")) {
							if (element.isSelected())
								element.click();
							Thread.sleep(2000);
							element.click();
							System.out.println("Executed :checkCheckBox, PASS");
							return true;
						} else {
							return false;
						}
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
		
	 /** Verifies if the webelement is of checkbox type and is checked
	 * @param element -the webelement of checkbox type 
	 * @return returns true of the webelement is of check type and is checked
	 * @throws IOException
	 */
	public boolean verifyCheckBox_Checked(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						if (element.getAttribute("type").equalsIgnoreCase("checkbox")) {
							if (element.isSelected())
								return true;
							else
								return false;
						} else {
							return false;
						}
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	 /** Verifies if the webelement is of checkbox type and is unchecked
		 * @param element -the webelement of checkbox type
		 * @return returns true of the webelement is of check type and is unchecked
		 * @throws IOException
		 */
	public boolean verifyCheckBox_UnChecked(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						if (element.getAttribute("type").equalsIgnoreCase("checkbox")) {
							if (!element.isSelected())
								return true;
							else
								return false;
						} else {
							return false;
						}
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	 
	 
	 /** Verifies if the webelement is of checkbox type and is unchecked
		 * @param element -the webelement of checkbox type
		 * @return returns true of the webelement is of check type and is unchecked
		 * @throws IOException
		 */
	public boolean uploadImageFile(WebElement element) throws IOException {
		int i = 0;
		try {
			for (i = 0; i < 7; i++) {
				try {
					(new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOf(element));
					if (element.isDisplayed()) {
						// ViewElement(element);
						if (element.getAttribute("type").equalsIgnoreCase("checkbox")) {
							if (!element.isSelected())
								return true;
							else
								return false;
						} else {
							return false;
						}
					} else {
						System.out.println("Trying to find object");
						continue;
					}
				} catch (TimeoutException | NoSuchElementException e) {
					System.out.println("Trying to find object");
					continue;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	 
	 public boolean uploadFile(String absolutePath){
			try {
				driver.manage().window().maximize();
				((JavascriptExecutor) driver).executeScript("window.focus();");
				StringSelection ss=new StringSelection(absolutePath);
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
				driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS) ;
				Robot robot=new Robot();
				robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
				robot.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
				robot.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
				robot.keyPress(java.awt.event.KeyEvent.VK_V);
				robot.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
				driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
				robot.keyPress(java.awt.event.KeyEvent.VK_ENTER);
				return true;
			} catch (HeadlessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			
		}
	 
	 
		/**
		 * Clicks on the webelement
		 * @param element webelement on which the click has to be performed
		 * @return if the element is visible on screen it will be clicked and returns true
		 * @throws IOException
		 * @throws InterruptedException 
		 */
		public boolean VerifyFileDownloaded(String Dir,String extension) throws IOException, InterruptedException
		{	
			int iterationLimitFileSearch=5;
			int fileSearchCtr=0;
			
			int fileCountBefore=getFilesInDirWithExt(Dir, extension);
			int fileCountAfter;
			do{
				System.out.println("Trying to serach for a new:\""+ extension+"\" file in directory\""+Dir+"\"");
				driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS) ;
				fileCountAfter=getFilesInDirWithExt(Dir, extension);
				fileSearchCtr++;
			}while(fileCountBefore==fileCountAfter && iterationLimitFileSearch>=fileSearchCtr);
			
			if(fileCountBefore<fileCountAfter)
				return false;
			return true;	
					
	 
}
		
		public File clickDownloadElementAndGetFileFromDownloads(WebElement element,String extension) throws IOException, InterruptedException  
		{	boolean clickFlag=false;
			int iterationLimitFileSearch=5;
			int fileSearchCtr=0;
			int fileCountAfter=0;
			int fileCountBefore=0;
			ArrayList<File> filesPrev=new ArrayList<File>();
			ArrayList<File> filesNew=new ArrayList<File>();
			try {
				String downloadsPath=System.getenv("USERPROFILE") + "\\Downloads";
				filesPrev.addAll(getAllFilesThatMatchFilenameExtension(downloadsPath,"*."+extension));
				
				fileCountBefore=getFilesInDirWithExt(downloadsPath, extension);
				clickFlag=clickElement(element);
				if(clickFlag==false){
					System.out.println("Unable to click on element to download the file");
					return null;
				}
				
				do{
					System.out.println("Trying to search for a new" +extension+" file:\""+ downloadsPath+"\" file in directory ");
					driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS) ;
					fileCountAfter=getFilesInDirWithExt(downloadsPath, extension);
					fileSearchCtr++;
				}while(fileCountBefore==fileCountAfter && iterationLimitFileSearch>=fileSearchCtr);
				
				if(fileCountBefore<fileCountAfter){
					System.out.println("No File downloaded with extension "+extension+" is created in directory:"+downloadsPath);
					return null;
				}
				filesNew.addAll(getAllFilesThatMatchFilenameExtension(downloadsPath,"*."+extension));
				filesNew.removeAll(filesPrev);
				System.out.println("Executed :verifyExcelFileDownloadedInDownloads, PASS");
				return filesNew.get(0);
				
				
			
			} catch (Exception e) {
				return null;
			}
		}
		
		
		
		public boolean verifyCompareStringArrays(String []arrayToCompareInUpperCase, String []arrayBaseInUpperCase) throws IOException {
			try {
				
				HashSet<String> tierset=new HashSet<>(Arrays.asList(arrayToCompareInUpperCase));
				
				for (String itr:arrayBaseInUpperCase ) {
					
					if(tierset.contains(itr.replaceAll(" ", "").toUpperCase())){
						
						System.out.println("header Present"+itr);
						tierset.remove(itr.replaceAll(" ", "").toUpperCase());
						
					}
				}
				
				if(tierset.size()==0){
					System.out.println("Executed: verifyCompareStringArrays, PASS");
					return true;
					
				}
				return false;
			}catch (Exception e) {
				
				return false;
			}
		}
		
		/**returns  the select webelement's selected value 
		 * @param element
		 * @param text
		 * @return returns true if the selected option text matches with given text as a parameter
		 * @throws IOException
		 */
		public String getSelectListSelectedValue(WebElement element) throws IOException
		{
			int i=0;
			try
			{	for(i=0;i<7;i++){
				try {
					(new WebDriverWait(driver,5)).until(ExpectedConditions.visibilityOf(element));
						if(element.isDisplayed()){
							//ViewElement(element);
							Select elementSelectObj = new Select(element);
							String selectedText=elementSelectObj.getFirstSelectedOption().getText();
							return selectedText;
								
						}
						else{
							System.out.println("Trying to find object");continue;
						}
					} catch (TimeoutException|NoSuchElementException e) {
						System.out.println("Trying to find object");continue;	
					}
				}
			}
			catch(Exception e)
			{ return null;   
			}
			return null;
		}
		


		/**returns  array of Strings containing the Select list options
		 * @param element
		 * @param text
		 * @return returns array of Strings containing the Select list options
		 * @throws IOException
		 */
		public ArrayList<String> getSelectListOptionValues(WebElement element) throws IOException
		{
			ArrayList<String> allOptions=new ArrayList<String>();
			int i=0;
			try
			{	for(i=0;i<7;i++){
				try {
					(new WebDriverWait(driver,5)).until(ExpectedConditions.visibilityOf(element));
						if(element.isDisplayed()){
							//ViewElement(element);
							Select elementSelectObj = new Select(element);
							for(WebElement itrOptions:elementSelectObj.getOptions()){
								allOptions.add(itrOptions.getText());
							}
							return allOptions;
								
						}
						else{
							System.out.println("Trying to find object");continue;
						}
					} catch (TimeoutException|NoSuchElementException e) {
						System.out.println("Trying to find object");continue;	
					}
				}
			}
			catch(Exception e)
			{ return null;   
			}
			return null;
		}
		public void navigate()
		{
			driver.navigate().back();
		}
		
		
		//today
		/**
		 * Selects the drop down value by value
		 * @param element	: the web element which is expected to be of select type
		 * @param text		: the value present in the select drop down
		 * @return true		: if the given value optionn is selected the return value is true else it returns false
		 * @throws			: IOException
		 */
		public boolean selectDropdownByValue(WebElement element, String value) throws IOException {
			int i = 0;
			try {
				for (i = 0; i < 7; i++) {
					try {
						(new WebDriverWait(driver, 5)).until(ExpectedConditions.visibilityOf(element));
						if (element.isDisplayed() && element.isEnabled()) {
							// ViewElement(element);
							Select elementSelectObj = new Select(element);
							if (!(elementSelectObj.getOptions().size() > 1))
								driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
								elementSelectObj.selectByIndex(Integer.parseInt(value));
								return true;
								
/*								if(!elementSelectObj.getFirstSelectedOption().getAttribute("value").equals(value)){
									System.out.println("The given value:"+value +" is unable to select");
								continue;
								}else{									
									return true;
								}*/
							}
						else {
							System.out.println("Trying to find object");
							continue;
						}
					} catch (TimeoutException | NoSuchElementException e) {
						System.out.println("Trying to find object - Timeout or NoSuch Element");
						continue;
					}
				}
			} catch (Exception e) {
				return false;
			}

			return false;
		}
		

		public String checkElementColor(WebElement we)
		{
			
			String color=we.getCssValue("background-color");
			//String color=we.getCssValue("border-color");
		    
			String temp=color.substring(5,color.length()-1);
			
			//System.out.println(temp);
			
			String temp1[]=temp.split(", ");
			
			int Red=Integer.parseInt(temp1[0]);
			
			int Green=Integer.parseInt(temp1[1]);
			
			int Blue=Integer.parseInt(temp1[2]);
			
			float opacity=Float.parseFloat(temp1[3]);
			
			if(opacity>0)
			{
			 if((Red>Green)&&(Red>Blue))
			{
				return "red";
			}
			 if((Green>Red)&&(Green>Blue))
				{
				 return "green";
				}
			 if((Blue>Red)&&(Blue>Green))
				{
				 return "blue";
				}
			return "unpredictable";
			}
			else if(opacity==0)
			
			{return "not colored";
			}
			
			return null;
			
		
	}
		    
}

