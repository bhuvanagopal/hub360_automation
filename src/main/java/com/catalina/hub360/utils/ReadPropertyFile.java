package com.catalina.hub360.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ReadPropertyFile {
	File file;
	FileInputStream fileinput;
	Properties prop;
	String rootPath;
	String envrn;

	public ReadPropertyFile() throws IOException
	{
		rootPath = System.getProperty("user.dir");

		file = new File(rootPath+"//hub360.properties");

		fileinput = null;

		try
		{
			fileinput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		prop = new Properties();
		prop.load(fileinput);
	}

	public String getURL() {
		return prop.getProperty("UATUrl");
	}

	public String getCatalinaUserName() {
		return prop.getProperty("CatalinaUser");
	}

	public String getCatalinaPassword() {
		return prop.getProperty("CatalinaPassword");
	}

	public String getErrorMessageColor() {
		return prop.getProperty("ErrorMessageColor");
	}

	public String getExistingBuyersColor() {
		return prop.getProperty("ExistingBuyersColor");
	}

	public String getNewToBrandColor() {
		return prop.getProperty("NewToBrandColor");
	}

	public String getNewToBrandAndCategoryColor() {
		return prop.getProperty("NewToBrandAndCategoryColor");
	}

	public String getAdminUserName() {
		return prop.getProperty("AdminUser");
	}

	public String getAdminPassword() {
		return prop.getProperty("AdminPassword");
	}

	public String getDbJdbcDriver() {
		return prop.getProperty("JdbcDriver");
	}

	public String getdbURL_Op() {
		return prop.getProperty("DbUrl_Op");
	}

	public String getdbUsername_Op() {
		return prop.getProperty("DbUserName_Op");
	}

	public String getActiveClient() {
		return prop.getProperty("ActiveClient");
	}

	public String getdbPassword_Op() {
		return prop.getProperty("DbPassword_Op");
	}

	public String getTestPlanId() {
		return prop.getProperty("TestPlanId");
	}

	public void setBuildID(String buildID) {
		prop.setProperty("BuildId", buildID);
	}

	public int getBuildID() {
		String build = new String();
		build = prop.getProperty("BuildId");
		return Integer.parseInt(build);
	}

	public String getBuildName() {
		System.out.println("Inside getBuildName");

		if (System.getProperty("BuildName") == null) {
			return prop.getProperty("BuildName");
		} else {
			return System.getProperty("BuildName");
		}

	}

	public String getTestPlanID() {
		if (System.getProperty("TestPlanID") == null) {
			return prop.getProperty("TestPlanId");
		} else {
			return System.getProperty("TestPlanID");
		}
	}

	public String getEnvironment(String env) {
		String url = null;
		
			if(env.equalsIgnoreCase("ProdUrl"))
			{
				url = prop.getProperty("ProdUrl");
			}
			else if(env.equalsIgnoreCase("SqaUrl"))
			{
				url =  prop.getProperty("SQAUrl");
			}
			else if(env.equalsIgnoreCase("UatUrl"))
			{
				url =  prop.getProperty("UATUrl");
			}
			System.out.println(url);
			return url;
	}

}
