package com.catalina.hub360.utils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.cucumber.listener.Reporter;
import com.google.common.io.Files;

public class ScreenShot {
	
	public static void addScreenshotToScreen(WebDriver driver, String imageName) throws InterruptedException, IOException {  
//		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
		File sourcePath = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		
File destination = new File(System.getProperty("user.dir")
		+ "/target/screenshots");


		if (!destination.exists()) {
			System.out.println("File created " + destination);
			destination.mkdir();
		}

		File destinationPath = new File(System.getProperty("user.dir")
				+ "/target/screenshots/"
				+ imageName + ".png");
		
		Files.copy(sourcePath, destinationPath);
		
		Reporter.addStepLog("<img src=System.getProperty(\"user.dir\")+\"/target/screenshots/"
				+ imageName + ".png"+"\" style=\"height:600px;width:100%\">");
	}
}
