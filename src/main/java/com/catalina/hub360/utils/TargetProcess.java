package com.catalina.hub360.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import br.eti.kinoshita.testlinkjavaapi.model.Attachment;
import entities.TestCase;
import entities.TestCaseRun;
import entities.TestPlan;
import entities.TestPlanRun;

public class TargetProcess {

	public static String TOKEN = "NTg5OmNSaTF2bW9BUGg3RTRnL1lEZFNzRUZvRWpNYzMzZE9ZcUhGVkN4cnoyYnM9";
	SharedResource sharedResource;
	public int testPlanRunID = 0;

	public Response createTestCase(TestCase testCase) {

		Client client = ClientBuilder.newClient();
		return client.target("https://catalina.tpondemand.com/api/v1/TestCases?access_token=" + TOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(testCase, MediaType.APPLICATION_JSON));
	}

	public int createTestPlanRun(TestPlanRun testPlanRun) {
		Client client = ClientBuilder.newClient();
		Response response = client.target("https://catalina.tpondemand.com/api/v1/TestPlanRuns?access_token=" + TOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(testPlanRun, MediaType.APPLICATION_JSON));
		String data = response.readEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonObject object = (JsonObject) parser.parse(data);
		System.out.println("Test Plan Run Created with id " + object.get("Id").getAsInt());
		return object.get("Id").getAsInt();
	}

	public int getTestCaseRunId(int testPlanRunId, int testCaseId) {
		String url = "https://catalina.tpondemand.com/api/v1/TestCaseRuns/?access_token=" + TOKEN
				+ "&where=(TestCase.ID eq " + testCaseId + ") and(TestPlanRun.ID eq " + testPlanRunId + ")";
		Client client = ClientBuilder.newClient();
		Response response = client.target(url.replace(" ", "%20")).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		String data = response.readEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonObject object = ((JsonObject) parser.parse(data)).get("Items").getAsJsonArray().get(0).getAsJsonObject();
		return object.get("Id").getAsInt();
	}

	public int updateStatus(int testPlanRunId, int testCaseId, TestCaseRun testCaseRun) {
		int testCaseRunId = getTestCaseRunId(testPlanRunId, testCaseId);
		Client client = ClientBuilder.newClient();
		Response response = client
				.target("https://catalina.tpondemand.com/api/v1/TestCaseRuns/" + testCaseRunId + "?access_token="
						+ TOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(testCaseRun, MediaType.APPLICATION_JSON));
		String data = response.readEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonObject object = (JsonObject) parser.parse(data);
		return object.get("Id").getAsInt();
	}
	
	public void updateExtentReport(int testPlanRunId, File file) {
		Client client = ClientBuilder.newClient();
		System.out.println("https://catalina.tpondemand.com/api/v1/Generals/" + testPlanRunId + "/attachments?access_token="
				+ TOKEN);
		Response response = client
				.target("https://catalina.tpondemand.com/api/v1/Generals/" + testPlanRunId + "/attachments?access_token="
						+ TOKEN)
				.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
				.post(Entity.entity(file, MediaType.APPLICATION_JSON));
	}
	
	public Response uploadAttachment(File file, int attchedWith) {
		
				//File file = new File(attachment.getFileName());
//				try {
//					FileOutputStream fos = new FileOutputStream(file);
//					fos.write(Base64.decodeBase64(attachment.getContent().getBytes()));
//				} catch (FileNotFoundException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
		
				Client client = ClientBuilder.newBuilder()
						.register(MultiPartFeature.class).build();
				WebTarget webTarget = client
						.target("https://catalina.tpondemand.com/UploadFile.ashx?access_token="
								+ TOKEN);
		
				FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("file", file,
						MediaType.APPLICATION_OCTET_STREAM_TYPE);
		
				MultiPart multiPart = new FormDataMultiPart().field("generalId",
						String.valueOf(attchedWith)).bodyPart(fileDataBodyPart);
		
				return webTarget.request(MediaType.APPLICATION_JSON)
						.accept(MediaType.APPLICATION_JSON)
						.post(Entity.entity(multiPart, MediaType.MULTIPART_FORM_DATA));
			}

	public List<Integer> bulkUpdate(int testPlanRunId, Map<Integer, TestCaseRun> map) {
		List<Integer> list = new ArrayList<>();

		for (Map.Entry entry : map.entrySet()) {
			int id = (int) entry.getKey();
			TestCaseRun testCaseRun = (TestCaseRun) entry.getValue();
			list.add(updateStatus(testPlanRunId, id, testCaseRun));

		}
		return list;
	}

	public int createBuild(ExtentReports report, int testPlanId, String buildName) {
		// TODO Auto-generated method stub
       //**************This Method creates target process build and returns the builId***************//
		TargetProcess targetProcess = new TargetProcess();
		ExtentTest initializationTest = null;
		if (report != null)
			initializationTest = report.startTest("Creating TestPlan Run");

		// int testPlanId = Integer.parseInt(properties.getProperty("Test_Plan_ID"));
		// String buildName = properties.getProperty("Build_Name");
		TestPlanRun testPlanRun = new TestPlanRun();
		testPlanRun.setName(buildName);
		testPlanRun.setTestPlan(new TestPlan(testPlanId));
		if(getBuildId(buildName, testPlanId)==-1)
			testPlanRunID = createTestPlanRun(testPlanRun);
		if (report != null)
			initializationTest.log(LogStatus.INFO,
					"Test Plan Run is created for with name " + buildName + " and id is " + testPlanRunID);

		return testPlanRunID;
	}

	
	public void bulkUpdate(List<Integer> testCaseIds, String status, String comment, ExtentTest test) {
		String buildName = null;
		int testPlanId = 0;
		// TODO Auto-generated method stub
		if (test != null)
			test.log(LogStatus.INFO,
					"Bulk Updating Result for TestPlanRun " + testPlanRunID + "<br>Status = " + status);

		
		try {
			//this.testPlanRunID = this.getBuildId(createTargetProcessBuild.get_the_build_name(buildName), Integer.parseInt((createTargetProcessBuild.get_the_Testplan_id(testPlanId))));
		} catch (Throwable e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Map<Integer, Integer> testCaseIdsforMapping = null;

		try {
			testCaseIdsforMapping = new BufferedReader(new FileReader(new File("testlinkAndTargetProcessId"))).lines()
					.map(p -> p.trim().split("\t"))
					.collect(Collectors.toMap(p -> Integer.parseInt(p[1]), p -> Integer.parseInt(p[0]), (old, news) -> {
						System.out.println(old + " "+ news);
						return old;
					}));
		} catch (NumberFormatException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int testCaseId : testCaseIds) {

			TestCaseRun testCaseRun = new TestCaseRun(status, comment);
			try {
				System.out.println("Updating Test Case under testbuild " + testPlanRunID + " testcase id "
						+ testCaseIdsforMapping.get(testCaseId));
				updateStatus(testPlanRunID, testCaseIdsforMapping.get(testCaseId), testCaseRun);
			} catch (Exception exception) {
				System.out.println(exception);
				System.out.println("Not updated test case " + testCaseId);
			}
		}

	}

	public int getBuildId() {
		// TODO Auto-generated method stub
		return this.testPlanRunID;
	}

	public int getBuildId(String buildName, int testPlanId) {
		String url = "https://catalina.tpondemand.com/api/v1/TestPlans/" + testPlanId + "/TestPlanRuns?access_token="
				+ TOKEN + "&where=(name eq '" + buildName + "')";
		Client client = ClientBuilder.newClient();
		Response response = client.target(url.replace(" ", "%20")).request(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON).get();
		String data = response.readEntity(String.class);
		JsonParser parser = new JsonParser();
		JsonArray array = ((JsonObject) parser.parse(data)).get("Items").getAsJsonArray();
		if(array.size()==0)
			return -1;
		else
			this.testPlanRunID = array.getAsJsonArray().get(0).getAsJsonObject().get("Id").getAsInt();
		return this.testPlanRunID;
	}

}
