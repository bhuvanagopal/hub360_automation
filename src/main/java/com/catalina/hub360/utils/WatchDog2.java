package com.catalina.hub360.utils;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.TimeUnit;
public class WatchDog2 {


	public static String getDownloadedDocumentName(String fileExtension) throws IOException {		
		
		String downloadedFileName = null;
		boolean valid = true;
		boolean found = false;
		
		String downloadDir;
		String falseFalg="false";
//	    NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("Catalina", "csauto4", "Catalina123");

		// default timeout in seconds
		long timeOut = 150;
		System.out.println("download");
		 
		try {
			
			System.out.println("debug--1");
			
		    String SystemOS= System.getProperty("os.name");
		    System.out.println("SystemOS--"+System.getProperty("os.name"));		    
		    
		    	//downloadDir = "\\\\10.22.6.178\\sjcfs\\public\\downloads";
		    	downloadDir = "C:\\Users\\csauto4\\Downloads\\";
		    
			
			System.out.println("Downlaod directory from WatchDog2 Service--"+downloadDir);					
			Path downloadFolderPath = Paths.get(downloadDir);
			
			System.out.println("downloadFolderPath--"+downloadFolderPath);
			
			WatchService watchService = FileSystems.getDefault().newWatchService();
			downloadFolderPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);			
			System.out.println("AFter Watch Service--");
			
			long startTime = System.currentTimeMillis();			
			System.out.println("startTime--"+startTime);
			
			do {
				WatchKey watchKey;		
				watchKey = watchService.poll(timeOut, TimeUnit.SECONDS);			
				System.out.println("WathcKey--"+watchKey);
				
				long currentTime = (System.currentTimeMillis() - startTime) / 1000;				
				System.out.println("Current Time--"+currentTime);
				
				if (currentTime > timeOut) {
					System.out.println("Debug before timeout--");
					System.out.println("Download operation timed out.. Expected file was not downloaded");
					return downloadedFileName;
				}

				System.out.println("Inside Watch Event Service Before For Loop--");

				
				
				for (WatchEvent<?> event : watchKey.pollEvents()) {
					Kind<?> kind = event.kind();
					System.out.println("Inside Watch Event Service--");
					if (kind.equals(StandardWatchEventKinds.ENTRY_CREATE)) {
						String fileName = event.context().toString();
						System.out.println("New File Created:" + fileName);
						if (fileName.endsWith(fileExtension)) {
						   
								downloadedFileName = fileName;
							
							//downloadedFileName = downloadDir + fileName;							
							System.out.println("Download File Name--"+downloadedFileName);
							System.out.println("Downloaded file found with extension " + fileExtension+ ". File name is " + fileName);
							Thread.sleep(500);
							found = true;
							break;
						}
					}
				}
				if (found) {
					System.out.println("Downloaded File is Found---");
					System.out.println("Downloaded File Name From WatchDog2 Service--"+downloadedFileName);
					return downloadedFileName;
				} else {
					currentTime = (System.currentTimeMillis() - startTime) / 1000;
					if (currentTime > timeOut) {
						System.out.println("Failed to download expected file");
						return downloadedFileName;
					}
					valid = watchKey.reset();
				}
			} while (valid);
		}

		catch (InterruptedException e) {
			System.out.println("Interrupted error - " + e.getMessage());
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.out.println("Download operation timed out.... Expected file was not downloaded");
		} catch (Exception e) {
			System.out.println("Error occured - " + e.getMessage());
			e.printStackTrace();
		}
		return downloadedFileName;
	}

}
