package entities;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TestPlanRun {

	
	String name;
	TestPlan testPlan;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TestPlan getTestPlan() {
		return testPlan;
	}

	public void setTestPlan(TestPlan testPlan) {
		this.testPlan = testPlan;
	}

	public TestPlanRun() {
	}
	
	

}
