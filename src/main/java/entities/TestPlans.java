package entities;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class TestPlans {
	
	List<TestPlan> items;

	public List<TestPlan> getItems() {
		return items;
	}

	public void setItems(List<TestPlan> items) {
		this.items = items;
	}

	public TestPlans() {
		this.items = new ArrayList<>();
	}

	
}
