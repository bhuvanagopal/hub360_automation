package runner;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.catalina.hub360.utils.ReadPropertyFile;
import com.catalina.hub360.utils.TargetProcess;
import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {
		"features/Login.feature",
		"features/MultiTouchAttributR.feature",
		"features/Brands.feature"
}, glue = { "com.catalina.hub360.steps",
"com.catalina.hub360.utils" }, plugin = {"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/ExtentReport.html"}
,tags ="@regression"
) 

public class TestRunner {

	private static final TargetProcess tp = new TargetProcess();
	public int testPlanRunID = 0;
	private static int testPlanId = 0;
	static String buildName = null;
	static ReadPropertyFile readPropertyFile;
	public static int buildID = -1;

	@BeforeClass
	public static void createBuild() {

		try{

			readPropertyFile = new ReadPropertyFile();
			
		    testPlanId = Integer.parseInt(readPropertyFile.getTestPlanID());
		    buildName = readPropertyFile.getBuildName();
			System.out.println(testPlanId+" "+buildName);

			System.out.println("testPlanId:::::::::::"+testPlanId);
			System.out.println("Build Name:::::::"+buildName);	


			buildID =tp.createBuild(null, testPlanId, buildName);
			System.out.println("Build Number:::::"+tp.getBuildId());

		}catch (Exception e){
			System.out.println("Could not create the build"+e.getMessage());
		}

	}

	@AfterClass
	public static void writeExtentReport() throws Exception {
		Reporter.loadXMLConfig(new File("./src/test/resources/extent-config.xml"));
		Reporter.getExtentReport().flush();
		tp.uploadAttachment(new File("target/cucumber-reports/ExtentReport.html"), buildID);
	}

}
